package com.example.zzalit_bigs;

import java.util.LinkedList;
import java.util.Vector;

import android.content.Intent;
import android.util.Log;

import com.example.zzalit_bigs.YTVideoSourceCrawler.YTURLCrawlingListener;

public class YTVideo {
	// YOUTUBE_BASE_URL+id -> youtube watching url
	private static final String YOUTUBE_BASE_URL = "http://www.youtube.com/watch?v=";
	private String id = null;
	private String title = null;
	private long duration = 0;
	private String thumb_url = null;
	private Vector<YTVideoSource> videoSources = null;
	
	
	public YTVideo(String id, String title, long duration, String thumb_url){
		this.id = id;
		this.title = title;
		this.duration = duration;
		this.thumb_url = thumb_url;
	}
	
	public String getId() {
		return id;
	}
	public void setId(String id) {
		this.id = id;
	}
	public String getTitle() {
		return title;
	}
	public void setTitle(String title) {
		this.title = title;
	}
	public long getDuration() {
		return duration;
	}
	public void setDuration(long duration) {
		this.duration = duration;
	}
	public String getThumb_url() {
		return thumb_url;
	}
	public void setThumb_url(String thumb_url) {
		this.thumb_url = thumb_url;
	}
	/**
	 * 
	 * @param maxQuality maxQuality이하의 영상을 찾음
	 * @param streamable streaming 가능한것만 찾음
	 * @return 조건에 맞는 영상소스를 찾을 수 없을경우 null, 있으면 그 url, 단 crawlVideoResources가 완료되지 않았을경우에도 null이 리턴된다.
	 */
	public String getProperSourceUrl(int maxQuality, boolean streamable){
		String url = null;
		if(videoSources == null)
			return null;
		for(YTVideoSource src : videoSources){//src는 quality에 따라 내림차순 정렬 되어있다.
			int quality = Integer.parseInt(src.getsQuality());
			int itag = Integer.parseInt(src.getsITAG());
			String format = src.getsTYPE();
			if( quality <= maxQuality && quality > 0 &&
					(streamable ? format.contains("mp") && itag < 100 : true)){
				url = src.getsUrl();
				break;
			}
		}	
		return url;
	}
	public Vector<YTVideoSource> getVideoSources(){
		return videoSources;
	}
	public boolean crawlVideoResources(boolean block, final YTURLCrawlingListener onCompleteListener){
		if(videoSources != null){
			if(onCompleteListener != null)
				onCompleteListener.onCompleted(videoSources);
			return false;
		}
		if(id != null){
			YTVideoSourceCrawler crawler = new YTVideoSourceCrawler(YOUTUBE_BASE_URL+id);
			crawler.setCrawlingListener(new YTURLCrawlingListener() {
				@Override
				public void onCompleted(Vector<YTVideoSource> ytvideos) {
					// TODO Auto-generated method stub
					Log.d("d","crawl complete cnt : "+ytvideos.size());
					videoSources = ytvideos;
					if(onCompleteListener != null)
						onCompleteListener.onCompleted(videoSources);
				}
			});
			crawler.start();
			if(block){
				try {
					crawler.join();
				} catch (InterruptedException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
			}
			return true;
		}
		return false;
	}
	
}
class YTVideoList{//for springspice
	public LinkedList<YTVideo> videos = new LinkedList<YTVideo>(); 
}