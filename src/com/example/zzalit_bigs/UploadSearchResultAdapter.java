package com.example.zzalit_bigs;

import java.util.LinkedList;
import java.util.List;
import java.util.Vector;

import android.content.Context;
import android.content.Intent;
import android.content.res.Resources;
import android.graphics.Bitmap;
import android.graphics.drawable.BitmapDrawable;
import android.graphics.drawable.ColorDrawable;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;

import com.actionbarsherlock.app.SherlockFragmentActivity;
import com.example.zzalit_bigs.YTVideoSourceCrawler.YTURLCrawlingListener;
import com.nostra13.universalimageloader.core.DisplayImageOptions;
import com.nostra13.universalimageloader.core.ImageLoader;
import com.nostra13.universalimageloader.core.assist.FailReason;
import com.nostra13.universalimageloader.core.assist.ImageLoadingListener;
import com.nostra13.universalimageloader.utils.L;

public class UploadSearchResultAdapter extends BaseAdapter{
	private LinkedList<YTVideo> results;
	private LayoutInflater inflater;
	private SherlockFragmentActivity activity;
	private DisplayImageOptions thumbDisplayOptions = null;
	private ZoopLoadingDialogFragment loadingDialog = null;
	
	public UploadSearchResultAdapter(SherlockFragmentActivity activity){
		this.activity = activity;
		results = new LinkedList<YTVideo>();
		setDisplayImageOptions();
		inflater = (LayoutInflater)activity.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
		Resources res = activity.getResources();
		loadingDialog = ZoopLoadingDialogFragment.newInstance(res.getDimensionPixelSize(R.dimen.upload_loading_bg_width), res.getDimensionPixelSize(R.dimen.upload_loading_bg_height),
				res.getDrawable(R.drawable.upload_loading_bg_2), res.getDrawable(R.drawable.upload_loading_wheel),
				res.getDimensionPixelSize(R.dimen.upload_loading_rotating_image_size), res.getDimensionPixelSize(R.dimen.upload_loading_rotating_image_size));
	}

	public void addResults(List<YTVideo> videos){
		results.addAll(videos);
		notifyDataSetChanged();
	}

	public void clear(){
		results.clear();
		notifyDataSetChanged();
	}

	@Override
	public int getCount() {
		// TODO Auto-generated method stub
		return results.size();
	}

	@Override
	public Object getItem(int arg0) {
		// TODO Auto-generated method stub
		return results.get(arg0);
	}

	@Override
	public long getItemId(int position) {
		// TODO Auto-generated method stub
		return position;
	}

	@Override
	public View getView(int position, View convertView, ViewGroup parent) {
		Log.d("bigs",""+position);
		// TODO Auto-generated method stub
		if(convertView == null){
			convertView = inflater.inflate(R.layout.upload_search_item, parent, false);
		}
		TextView tvTitle = (TextView)convertView.findViewById(R.id.tv_upload_video_title);
		TextView tvTime = (TextView)convertView.findViewById(R.id.tv_upload_video_time);
		ImageView ivThumb = (ImageView)convertView.findViewById(R.id.iv_upload_video_thumnail);
		Button btGet = (Button)convertView.findViewById(R.id.bt_upload_get_video);

		final YTVideo video = results.get(position);
		tvTitle.setText(video.getTitle());
		long dur = video.getDuration();
		if(dur > 60*60)
			tvTime.setText(String.format("%d:%02d:%02d",
					dur/(60*60), (dur%(60*60))/60, (dur%(60*60))%60));
		else
			tvTime.setText(String.format("%02d:%02d",dur/60, dur%60));
		ImageLoader.getInstance().displayImage(video.getThumb_url(), ivThumb, thumbDisplayOptions,
				new ImageLoadingListener() {

			@Override
			public void onLoadingStarted(String imageUri, View view) {

			}
			@Override
			public void onLoadingFailed(String imageUri, View view,
					FailReason failReason) {
			}
			@Override
			public void onLoadingComplete(String imageUri, View view, Bitmap loadedImage) {
				((ImageView)view).setImageDrawable(new BitmapDrawable(activity.getResources(), loadedImage));
			}
			@Override
			public void onLoadingCancelled(String imageUri, View view) {

			}
		});
		btGet.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				if(video.getVideoSources() == null)
					loadingDialog.show(activity.getSupportFragmentManager(), "crawling_loading");
				video.crawlVideoResources(false, new YTURLCrawlingListener() {
					@Override
					public void onCompleted(Vector<YTVideoSource> ytvideos) {
						// TODO Auto-generated method stub
						if(loadingDialog.getDialog() != null)
							loadingDialog.dismiss();
						String url = video.getProperSourceUrl(480, true);
						if(url != null){
							Intent intent = new Intent(activity, UploadVideoCutActivity.class);
							intent.putExtra("urlForVideo", video.getProperSourceUrl(360, true));
							intent.putExtra("urlForThumbs", video.getProperSourceUrl(240, true));
							intent.putExtra("duration", video.getDuration());
							activity.startActivity(intent);
						}else{
							//적절한게 없다는 경고창 띄워주기
						}
					}
				});
				
			}
		});

		return convertView;
	}

	protected void setDisplayImageOptions() {
		if (thumbDisplayOptions == null) {
			thumbDisplayOptions = new DisplayImageOptions.Builder()
			.showImageOnLoading(new ColorDrawable(activity.getResources().getColor(R.color.gray_4)))
			.showImageForEmptyUri(new ColorDrawable(activity.getResources().getColor(R.color.gray_4)))
			.showImageOnFail(new ColorDrawable(activity.getResources().getColor(R.color.gray_4)))
			.cacheInMemory(true)
			.cacheOnDisc(true)
			.bitmapConfig(Bitmap.Config.RGB_565) // 용량을 1/2정도로 아낄 수 있지만, blur 효과가 이상하게 들어감.
			.build();
			L.disableLogging();
		}
	}
}
