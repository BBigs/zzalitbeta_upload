package com.example.zzalit_bigs;

import java.io.File;
import java.io.IOException;

import android.media.MediaPlayer;
import android.os.Bundle;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.View.OnTouchListener;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageButton;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.Toast;

import com.actionbarsherlock.app.SherlockFragment;
import com.actionbarsherlock.view.Menu;
import com.actionbarsherlock.view.MenuInflater;
import com.actionbarsherlock.view.MenuItem;
import com.example.zzalit_bigs.LoopingVideoView.OnPreparedListener;

public class UploadVideoConfigFragment extends SherlockFragment{

	public static final String TAG = "UploadVideoConfigFragment";
	//actionbar menu item
	private static final int ITEM_CONFIG_COMPLETE = 1;

	static public UploadVideoConfigFragment newInstance(String videoUrl, String jpegsDir){
		UploadVideoConfigFragment fragment = new UploadVideoConfigFragment();
		Bundle bundle = new Bundle();
		bundle.putString("url", videoUrl);
		bundle.putString("jpegsDir", jpegsDir);
		fragment.setArguments(bundle);
		return fragment;
	}

	private String videoUrl = null;
	private File jpegsDir = null;
	private RelativeLayout rootView = null;
	private Button btPlaySpeed = null;
	private ImageButton btPlayMode = null;
	private ImageButton btSoundConfig = null;
	private LoopingVideoView loopingVideo = null;

	private boolean isSoundOn = true;
	private boolean soundEnable = true;
	private MediaPlayer mp = null;

	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container,
			Bundle savedInstanceState) {
		// TODO Auto-generated method stub
        setHasOptionsMenu(true);
		videoUrl = getArguments().getString("url");
		jpegsDir = new File(getArguments().getString("jpegsDir"));
		rootView = (RelativeLayout)inflater.inflate(R.layout.fragment_upload_video_config, null);
		loopingVideo = (LoopingVideoView)rootView.findViewById(R.id.video_config_display);
		btPlaySpeed = (Button)rootView.findViewById(R.id.tv_play_speed);
		btPlayMode = (ImageButton)rootView.findViewById(R.id.bt_play_mode);
		btSoundConfig = (ImageButton)rootView.findViewById(R.id.bt_upload_sound_config);

		btPlaySpeed.setText(String.format("X%.1f", loopingVideo.getPlaySpeed()));

		rootView.setOnTouchListener(new OnTouchListener() {

			@Override
			public boolean onTouch(View v, MotionEvent event) {
				// TODO Auto-generated method stub
				return true;//뒤에 있는 프래그먼트가 터치를 먹지 않게.
			}
		});
		loopingVideo.setOnPreparedListener(new OnPreparedListener() {

			@Override
			public void onPrepared(MediaPlayer mp) {
				// TODO Auto-generated method stub
				UploadVideoConfigFragment.this.mp = mp;
				/*float scale = videoViewWrapper.getWidth()/(float)mp.getVideoWidth();
				int scaledHeight = (int) (mp.getVideoHeight()*scale);
				int configsHeight = getActivity().getResources().getDimensionPixelSize(R.dimen.upload_video_config_icon_size)+
						2*getActivity().getResources().getDimensionPixelSize(R.dimen.margin_medium);
				if(scaledHeight <= videoViewWrapper.getHeight()-configsHeight){//api의존적인 코드는 아닌지.
					LayoutParams params = videoViewWrapper.getLayoutParams();
					params.height = scaledHeight; //사실 params가 reference라서 set안해도 적용되긴함. for 안전
					videoViewWrapper.setLayoutParams(params);
				}else{
					LayoutParams params = videoViewWrapper.getLayoutParams();
					params.height = videoViewWrapper.getHeight()-configsHeight; //사실 params가 reference라서 set안해도 적용되긴함. for 안전
					videoViewWrapper.setLayoutParams(params);
				}*/
				loopingVideo.start(-1);
			}
		});
		btPlaySpeed.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				loopingVideo.setPlaySpeed(loopingVideo.getPlaySpeed()+0.5f);
				if(loopingVideo.getPlaySpeed() > LoopingVideoView.PLAY_SPEED_MAX)
					loopingVideo.setPlaySpeed(LoopingVideoView.PLAY_SPEED_MIN);
				if(loopingVideo.getPlaySpeed() == 1.0f){
					soundOff();
					soundEnable = true;
				}
				else
					soundDisable();
				btPlaySpeed.setText(String.format("X%.1f", loopingVideo.getPlaySpeed()));
			}
		});

		btPlayMode.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				switch(loopingVideo.getMode()){
				case LoopingVideoView.PLAY_MODE_NORMAL:
					btPlayMode.setImageResource(R.drawable.upload_play_mode_button_reverse);
					loopingVideo.setMode(LoopingVideoView.PLAY_MODE_REVERSE);
					soundDisable();
					break;
				case LoopingVideoView.PLAY_MODE_REVERSE:
					btPlayMode.setImageResource(R.drawable.upload_play_mode_button_pingpong);
					loopingVideo.setMode(LoopingVideoView.PLAY_MODE_PINGPONG);
					soundDisable();
					break;
				case LoopingVideoView.PLAY_MODE_PINGPONG:
					btPlayMode.setImageResource(R.drawable.upload_play_mode_button_normal);
					loopingVideo.setMode(LoopingVideoView.PLAY_MODE_NORMAL);
					soundOff();
					soundEnable = true;
					break;
				}
			}
		});
		
		btSoundConfig.setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				if(!soundEnable)
					return;
				if(isSoundOn){
					soundOff();
				}else{
					soundOn();
				}
			}
		});
		try {
			loopingVideo.setDataSource(videoUrl, jpegsDir.getAbsolutePath());
			loopingVideo.prepareAsync();
		} catch (IllegalArgumentException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (SecurityException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (IllegalStateException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return rootView;
	}
	@Override
	public void onDestroy() {
		// TODO Auto-generated method stub
		Toast.makeText(getActivity(), "destroy", Toast.LENGTH_SHORT).show();
		loopingVideo.recycle();
		Toast.makeText(getActivity(), "destroy_recycle", Toast.LENGTH_SHORT).show();
		super.onDestroy();
	}
	
	public void restore(){
		if(loopingVideo.getVisibility() != View.VISIBLE)
			loopingVideo.setVisibility(View.VISIBLE);
		loopingVideo.start(-1);//이전 상태로 플레이
	}
	public void recycle(){
		loopingVideo.recycle();
	}

	@Override
	public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
		// TODO Auto-generated method stub
		menu.clear();
		MenuItem itemComplete = menu.add(Menu.NONE, ITEM_CONFIG_COMPLETE, Menu.NONE, "cut_complete");
		itemComplete.setIcon(R.drawable.upload_video_complete_button);
		itemComplete.setShowAsAction(MenuItem.SHOW_AS_ACTION_ALWAYS);
	}
	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		// TODO Auto-generated method stub
		switch (item.getItemId()) {
		case ITEM_CONFIG_COMPLETE:
			Toast.makeText(getActivity(), "complete", Toast.LENGTH_SHORT).show();
			FragmentManager fm = getSherlockActivity().getSupportFragmentManager();
			fm.beginTransaction().setTransition(FragmentTransaction.TRANSIT_FRAGMENT_OPEN)
			.add(R.id.upload_fragment_frame, UploadPostFragment.newInstance(videoUrl), UploadPostFragment.TAG)
			.addToBackStack(null).commit();
			loopingVideo.pause();
//			loopingVideo.setVisibility(View.INVISIBLE);
			break;
		}
		return false;
	}
	private void soundOn(){
		isSoundOn = true;
		mp.setVolume(1, 1);
		btSoundConfig.setImageResource(R.drawable.upload_sound_on_button);

	}
	private void soundOff(){
		isSoundOn = false;
		mp.setVolume(0, 0);
		btSoundConfig.setImageResource(R.drawable.upload_sound_off_button);
	}
	private void soundDisable(){
		soundEnable = false;
		mp.setVolume(0, 0);
		btSoundConfig.setImageResource(R.drawable.upload_sound_disable_button);
	}
}
