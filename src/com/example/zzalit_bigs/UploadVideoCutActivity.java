package com.example.zzalit_bigs;

import java.io.File;
import java.io.IOException;

import android.content.Context;
import android.content.Intent;
import android.content.res.Resources;
import android.graphics.BitmapFactory;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.media.MediaPlayer;
import android.media.MediaPlayer.OnPreparedListener;
import android.os.Bundle;
import android.os.Environment;
import android.os.Handler;
import android.os.Looper;
import android.util.Log;
import android.view.MotionEvent;
import android.view.View;
import android.view.View.OnTouchListener;
import android.view.ViewGroup;
import android.view.animation.AlphaAnimation;
import android.view.animation.Animation;
import android.view.animation.Animation.AnimationListener;
import android.widget.ImageView;
import android.widget.SeekBar;
import android.widget.SeekBar.OnSeekBarChangeListener;
import android.widget.VideoView;

import com.actionbarsherlock.app.ActionBar;
import com.actionbarsherlock.app.SherlockFragmentActivity;
import com.actionbarsherlock.view.Menu;
import com.actionbarsherlock.view.MenuItem;
import com.example.zzalit_bigs.ThumbnailSeekBar.Belt;
import com.example.zzalit_bigs.ThumbnailSeekBar.OnSeekListener;

public class UploadVideoCutActivity extends SherlockFragmentActivity{

	public static final String TAG = "UploadVideoCutActivity";
	private static final int ITEM_CUT = 0;
	
	private static final long CUT_DURATION = 15000;

	//	static UploadVideoCutActivity newInstance(String videoUrl){
	//		UploadVideoCutActivity fragment = new UploadVideoCutActivity();
	//		Bundle bundle = new Bundle();
	//		bundle.putString("url", videoUrl);
	//		fragment.setArguments(bundle);
	//		return fragment;
	//	}

	private ImageView ivPlay = null;
	private ImageView ivPause = null;
	private VideoView videoView = null;
	private SeekBar sbCutVideo = null;
	private ThumbnailSeekBar sbThumbs = null;
	private AlphaAnimation animPlayImage = null;
	private AlphaAnimation animPauseImage = null;
	private MediaPlayer mp = null;
	private int prePlayTime = 0;
	private ViewGroup rootView = null;
	private ZoopLoadingDialogFragment loadingDialog = null;

	private String videoUrl = null;
	private String thumbsUrl = null;
	private long videoDuration = -1;
	private File thumbsTmpDir = null;
	private Handler timeHandler = null;
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_upload_video_cut);

		ActionBar actionBar = getSupportActionBar();
		actionBar.setDisplayHomeAsUpEnabled(true);
		actionBar.setDisplayShowHomeEnabled(false);
		actionBar.setBackgroundDrawable(new ColorDrawable(getResources().getColor(R.color.theme_blue)));
		actionBar.setTitle("업로드");

		Resources res = getResources();
		loadingDialog = ZoopLoadingDialogFragment.newInstance(res.getDimensionPixelSize(R.dimen.upload_loading_bg_width), res.getDimensionPixelSize(R.dimen.upload_loading_bg_height),
				res.getDrawable(R.drawable.upload_loading_bg_2), res.getDrawable(R.drawable.upload_loading_wheel),
				res.getDimensionPixelSize(R.dimen.upload_loading_rotating_image_size), res.getDimensionPixelSize(R.dimen.upload_loading_rotating_image_size));
		loadingDialog.show(getSupportFragmentManager(), "loading_video");

		videoUrl = getIntent().getStringExtra("urlForVideo");
		thumbsUrl = getIntent().getStringExtra("urlForThumbs");
		videoDuration = getIntent().getLongExtra("duration", -1);
		
		thumbsTmpDir = getDir("thumbs", Context.MODE_PRIVATE);
		File[] removinFiles = thumbsTmpDir.listFiles();
		for(File file : removinFiles)
			file.delete();
		
		
		rootView = (ViewGroup) findViewById(R.id.layout_upload_video_cut);
		sbThumbs = (ThumbnailSeekBar)rootView.findViewById(R.id.sb_video_cut_film_bar);
		sbCutVideo = (SeekBar) findViewById(R.id.sb_cut_video);
		videoView = (VideoView)rootView.findViewById(R.id.video_cut_display);
		ivPlay = (ImageView)rootView.findViewById(R.id.iv_video_cut_play);
		ivPause = (ImageView)rootView.findViewById(R.id.iv_video_cut_pause);
		sbCutVideo = (SeekBar)rootView.findViewById(R.id.sb_cut_video);
		
		timeHandler = new Handler(Looper.getMainLooper());
		//		videoView.setZOrderOnTop(true);
		
		animPlayImage = new AlphaAnimation(0.8f, 0.0f);
		animPlayImage.setDuration(800);
		animPauseImage = new AlphaAnimation(0.8f, 0.0f);
		animPauseImage.setDuration(800);

		rootView.setOnTouchListener(new OnTouchListener() {

			@Override
			public boolean onTouch(View v, MotionEvent event) {
				// TODO Auto-generated method stub
				return true;//뒤에 있는 프래그먼트가 터치를 먹지 않게.
			}
		});
		
		sbCutVideo.setOnSeekBarChangeListener(new OnSeekBarChangeListener() {
			
			@Override
			public void onStopTrackingTouch(SeekBar seekBar) {
				// TODO Auto-generated method stub
				
			}
			
			@Override
			public void onStartTrackingTouch(SeekBar seekBar) {
				// TODO Auto-generated method stub
				
			}
			
			@Override
			public void onProgressChanged(SeekBar seekBar, int progress,
					boolean fromUser) {
				if(fromUser && mp != null)
					videoView.seekTo(progress *1000);
				// TODO Auto-generated method stub
			}
		});
		videoView.setOnPreparedListener(new OnPreparedListener() {

			@Override
			public void onPrepared(MediaPlayer mp) {
				// TODO Auto-generated method stub
				UploadVideoCutActivity.this.mp = mp;
				sbCutVideo.setMax(mp.getDuration()/1000-1);
				videoView.setBackgroundColor(Color.TRANSPARENT);
				seekToVideo(prePlayTime);
				if(loadingDialog.getDialog() != null)
					loadingDialog.dismiss();
				playVideo();
				timeHandler.postDelayed(new Runnable() {
					
					@Override
					public void run() {
						// TODO Auto-generated method stub
						if(UploadVideoCutActivity.this.mp == null)
							return;
						if(!sbThumbs.isSeeking())//TODO 액티비티가 넘어가면  handler를 죽여야할거같은데
							sbThumbs.setCurrentTime(UploadVideoCutActivity.this.mp.getCurrentPosition(), 20);
						sbCutVideo.setProgress(UploadVideoCutActivity.this.mp.getCurrentPosition()/1000);
						timeHandler.postDelayed(this, 30);
					}
				}, 30);
			}
		});
		videoView.setOnTouchListener(new OnTouchListener() {

			@Override
			public boolean onTouch(View v, MotionEvent event) {
				// TODO Auto-generated method stub
				if(event.getAction() == MotionEvent.ACTION_DOWN){
					if(videoView.isPlaying())
						pauseVideo();
					else
						playVideo();
				}
				return true;
			}
		});
		videoView.setVideoPath(videoUrl);
		
		sbThumbs.setBelt(300);
		Belt belt = sbThumbs.getBelt();
		belt.setTopImage(BitmapFactory.decodeResource(getResources(), R.drawable.upload_cut_belt_top_pttn),
				getResources().getDimensionPixelSize(R.dimen.upload_video_cut_film_belt_bar_width));
		belt.setBottomImage(BitmapFactory.decodeResource(getResources(),R.drawable.upload_cut_belt_bottom), 
				getResources().getDimensionPixelSize(R.dimen.upload_video_cut_film_belt_bar_width));
		belt.setLeftImage(BitmapFactory.decodeResource(getResources(), R.drawable.upload_cut_belt_side),
				getResources().getDimensionPixelSize(R.dimen.upload_video_cut_film_belt_bar_width));
		belt.setRightImage(BitmapFactory.decodeResource(getResources(), R.drawable.upload_cut_belt_side),
				getResources().getDimensionPixelSize(R.dimen.upload_video_cut_film_belt_bar_width));
		belt.setShadow(getResources().getDimensionPixelSize(R.dimen.upload_video_cut_belt_shadow));
		sbThumbs.setOnSeekListener(new OnSeekListener() {
			
			@Override
			public void onSeeking(long seekingTime) {
				// TODO Auto-generated method stub
				if(mp != null){
					seekToVideo(seekingTime);
				}
			}
			
			@Override
			public void onSeeked(long seekedTime) {
				// TODO Auto-generated method stub
				Log.d("bigs", "onSeeked : "+seekedTime);
				if(mp != null){
					seekToVideo(seekedTime);
				}
			}
		});
		sbThumbs.post(new Runnable(){
			@Override
			public void run() {
				// TODO Auto-generated method stub
				sbThumbs.startExtractThumbs( thumbsTmpDir.getAbsolutePath(), thumbsUrl, videoDuration*1000, 100 , 5000);
			}
		});
	}
	
	private void playVideo(){
		videoView.start();
		ivPlay.setVisibility(View.VISIBLE);
		animPlayImage.setAnimationListener(new AnimationListener() {

			@Override
			public void onAnimationStart(Animation animation) {
				// TODO Auto-generated method stub

			}

			@Override
			public void onAnimationRepeat(Animation animation) {
				// TODO Auto-generated method stub

			}

			@Override
			public void onAnimationEnd(Animation animation) {
				// TODO Auto-generated method stub
				ivPlay.setVisibility(View.GONE);
			}
		});
		ivPlay.startAnimation(animPlayImage);
	}
	private void pauseVideo(){
		videoView.pause();
		ivPause.setVisibility(View.VISIBLE);
		animPauseImage.setAnimationListener(new AnimationListener() {

			@Override
			public void onAnimationStart(Animation animation) {
				// TODO Auto-generated method stub

			}

			@Override
			public void onAnimationRepeat(Animation animation) {
				// TODO Auto-generated method stub

			}

			@Override
			public void onAnimationEnd(Animation animation) {
				// TODO Auto-generated method stub
				ivPause.setVisibility(View.GONE);
			}
		});
		ivPause.startAnimation(animPauseImage);
	}
	private void seekToVideo(long time){
		mp.start();
		mp.seekTo((int)time);
	}
	public void restore(){
		if(videoView.getVisibility() != View.VISIBLE){
			videoView.setVisibility(View.VISIBLE);
		}
	}
	@Override
	protected void onPause() {
		// TODO Auto-generated method stub
		if(mp != null){
			prePlayTime = mp.getCurrentPosition();
		}
		super.onPause();
		mp = null;//TODO 임시
	}
	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		// TODO Auto-generated method stub
		menu.clear();
		MenuItem itemCut = menu.add(Menu.NONE, ITEM_CUT, Menu.NONE, "cut");
		itemCut.setIcon(R.drawable.upload_cut_video_button);
		itemCut.setShowAsAction(MenuItem.SHOW_AS_ACTION_ALWAYS);
		return true;
	}

	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		// TODO Auto-generated method stub
		switch(item.getItemId()){
		case ITEM_CUT :
			loadingDialog.show(getSupportFragmentManager(), "cuttring");
			if(videoView.isPlaying())
				pauseVideo();
			new Thread(){
				public void run() {
					File editVideo = new File(Environment.getExternalStorageDirectory(), "editing.mp4");// new File(getActivity().getDir("edit", Context.MODE_PRIVATE), "editing_video.mp4");
					File jpegsDir = new File(Environment.getExternalStorageDirectory(), "test");//getActivity().getDir("jpegs", Context.MODE_PRIVATE);
					if(!jpegsDir.exists())
						jpegsDir.mkdir();
					Log.d("bigs"," " + editVideo.getAbsolutePath()+" cut!");
					try {

						new VideoEditorBuilder(UploadVideoCutActivity.this)
						.setExtractTime(Math.max(Math.min(sbThumbs.getCurrentTime()/1000-3, videoDuration - CUT_DURATION/1000), 0),
								CUT_DURATION/1000)//벨트가 시작되기 3초전부터 cur_duration만큼 자름
						.onlyKeyFrame(true)
						.setFps(25)
						.build().edit(videoUrl, editVideo.getAbsolutePath());
						new VideoEditorBuilder(UploadVideoCutActivity.this)
						.setExtractTime(0, CUT_DURATION/1000)
						.setFps(25)
						.images(true)
						.build().edit(editVideo.getAbsolutePath(), jpegsDir.getAbsolutePath()+"/%d.jpeg");
					} catch (IOException e) {
						// TODO Auto-generated catch block
						e.printStackTrace();
					} catch (InterruptedException e) {
						// TODO Auto-generated catch block
						e.printStackTrace();
					}
					if(loadingDialog.getDialog() != null)
						loadingDialog.dismiss();
					Intent intent = new Intent(UploadVideoCutActivity.this, UploadVideoConfigActivity.class);
					intent.putExtra("url", editVideo.getAbsolutePath());
					intent.putExtra("jpegsDir", jpegsDir.getAbsolutePath());
					intent.putExtra("duration", CUT_DURATION);
					startActivity(intent);
					overridePendingTransition(android.R.anim.slide_out_right, android.R.anim.slide_in_left);

				};
			}.start();
			//			FragmentManager fm = getSherlockActivity().getSupportFragmentManager();
			//			fm.beginTransaction().setTransition(FragmentTransaction.TRANSIT_FRAGMENT_OPEN)
			//			.add(R.id.upload_fragment_frame, UploadVideoConfigFragment.newInstance(editVideo.getAbsolutePath(), jpegsDir.getAbsolutePath()), UploadVideoConfigFragment.TAG)
			//			.addToBackStack(null).commit();
			//			videoView.setVisibility(View.INVISIBLE);//surface가 겹치는걸 막기위해
			//			fm.beginTransaction().remove(this).commit();
		}
		return false;
	}

	@Override
	public void onDestroy() {
		// TODO Auto-generated method stub
		if(videoView.isPlaying())
			videoView.pause();
		super.onDestroy();
	}
}
