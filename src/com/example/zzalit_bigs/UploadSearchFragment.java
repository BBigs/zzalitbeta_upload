package com.example.zzalit_bigs;

import java.util.Vector;

import android.app.AlertDialog;
import android.app.Dialog;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.Display;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewOverlay;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.view.inputmethod.EditorInfo;
import android.view.inputmethod.InputMethodManager;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.MediaController;
import android.widget.TextView;
import android.widget.TextView.OnEditorActionListener;
import android.widget.VideoView;

import com.actionbarsherlock.app.SherlockFragment;
import com.haarman.listviewanimations.swinginadapters.prepared.SwingBottomInAnimationAdapter;
import com.handmark.pulltorefresh.library.PullToRefreshBase;
import com.handmark.pulltorefresh.library.PullToRefreshBase.Mode;
import com.handmark.pulltorefresh.library.PullToRefreshBase.OnRefreshListener;
import com.handmark.pulltorefresh.library.PullToRefreshListView;
import com.octo.android.robospice.JacksonSpringAndroidSpiceService;
import com.octo.android.robospice.SpiceManager;
import com.octo.android.robospice.persistence.DurationInMillis;
import com.octo.android.robospice.persistence.exception.SpiceException;
import com.octo.android.robospice.request.listener.RequestListener;

public class UploadSearchFragment extends SherlockFragment{
	public static final String TAG = "UploadSearchFragment";
	public static final int CNT_RESULTS_PER_REQUEST = 50;//1~50
	
	private SpiceManager spiceManager = null;
	
	private PullToRefreshListView lvSearchResults = null;
	private UploadSearchResultAdapter listAdapter = null;
	private ImageView ivResultEmpty = null;
	private ImageView ivSearchReady = null;
	
	private int lastIndex = 1-CNT_RESULTS_PER_REQUEST; //불러온 마지막 인덱스, index는 1부터 시작한다 
	private String query = "";
	public UploadSearchFragment(){
		super();
		spiceManager = new SpiceManager(JacksonSpringAndroidSpiceService.class);
	}
	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container,
			Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		//root
		View root = inflater.inflate(R.layout.fragment_upload_search, null);
		//ready, error imageView
		ivResultEmpty = (ImageView) root.findViewById(R.id.iv_upload_search_result_empty);
		ivSearchReady = (ImageView) root.findViewById(R.id.iv_upload_search_ready);
		
		//ListView
		lvSearchResults = (PullToRefreshListView)root.findViewById(R.id.lv_upload_search_result);
		listAdapter = new UploadSearchResultAdapter(this.getSherlockActivity());
		SwingBottomInAnimationAdapter swingAniAdapter = new SwingBottomInAnimationAdapter(listAdapter);
		lvSearchResults.setAdapter(swingAniAdapter);
		swingAniAdapter.setAbsListView(lvSearchResults.getRefreshableView());
		lvSearchResults.setPullToRefreshOverScrollEnabled(false);
		lvSearchResults.setMode(Mode.PULL_FROM_END);
		lvSearchResults.setOnRefreshListener(new OnRefreshListener<ListView>() {
			@Override
			public void onRefresh(PullToRefreshBase<ListView> refreshView) {
				// TODO Auto-generated method stub
				performRequest(query, lastIndex+=CNT_RESULTS_PER_REQUEST);
			}
		});
		lvSearchResults.getRefreshableView().setOnItemClickListener(new OnItemClickListener() {
			@Override
			public void onItemClick(AdapterView<?> parent, View view, int position,
					long id) {
				position -= 1;//이상하게 1높게 반환되네
				setHasOptionsMenu(true);
				Log.d(TAG,"on preview item click "+position);
				YTVideo video = (YTVideo) listAdapter.getItem(position);
				video.crawlVideoResources(true);
				String url = video.getProperSourceUrl(480, true);
				if(url == null){
					//재생할 만한 video format이 없음
					//TODO 경고창 띄워줘야함
					Log.d(TAG, "video dont have proper source");
				}else{
					Intent intent = new Intent(getActivity(), VideoPreViewActivity.class);
					intent.putExtra("url", url);
					startActivity(intent);
				}
//				new AlertDialog.Builder(getActivity())
//				.setView(videoView).show();
			}
		});
		//search
		final Button btSearch = (Button)root.findViewById(R.id.bt_upload_search);
		final EditText etSearch = (EditText)root.findViewById(R.id.et_upload_search);
		btSearch.setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				InputMethodManager imm = (InputMethodManager)getActivity().getSystemService(
					      Context.INPUT_METHOD_SERVICE);
				imm.hideSoftInputFromWindow(etSearch.getWindowToken(), 0);
				if(query.equals(etSearch.getText().toString()))
					return;
				listAdapter.clear();
				query = etSearch.getText().toString();
				performRequest(query, lastIndex+=CNT_RESULTS_PER_REQUEST);
			}
		});
		etSearch.setOnEditorActionListener(new OnEditorActionListener() {
			
			@Override
			public boolean onEditorAction(TextView v, int actionId, KeyEvent event) {
				// TODO Auto-generated method stub
				if(actionId == EditorInfo.IME_ACTION_SEARCH)
					btSearch.performClick();
				return false;
			}
		});
		return root;
	}
	@Override
	public void onStart() {
		// TODO Auto-generated method stub
		Log.d("test","f start");
		spiceManager.start(getActivity());
		super.onStart();
	}
	@Override
	public void onStop() {
		// TODO Auto-generated method stub
		Log.d("test","f stop");
		spiceManager.shouldStop();
		super.onStop();
	}
	@Override
	public void onDestroy() {
		// TODO Auto-generated method stub
		Log.d("test","f destroy");
		super.onDestroy();
	}

	public void performRequest(String query, int startIndex){
		VideoInfoRequest request = new VideoInfoRequest(YTVideoList.class, 
				query, startIndex, CNT_RESULTS_PER_REQUEST);
		spiceManager.execute(request, request.getCacheKey(),
				DurationInMillis.ONE_MINUTE*30, new VideoInfoRequestListener());//TODO 30분이나아니라 업로드 액티비티를 나갈때까지 할수없나
	}
	
	protected void showResultEmpty(){
		ivResultEmpty.setVisibility(View.VISIBLE);
		ivSearchReady.setVisibility(View.INVISIBLE);
		lvSearchResults.setVisibility(View.INVISIBLE);
	}
	protected void showSearchReady(){//default
		ivResultEmpty.setVisibility(View.INVISIBLE);
		ivSearchReady.setVisibility(View.VISIBLE);
		lvSearchResults.setVisibility(View.INVISIBLE);
	}
	protected void showResultList(){
		ivResultEmpty.setVisibility(View.INVISIBLE);
		ivSearchReady.setVisibility(View.INVISIBLE);
		lvSearchResults.setVisibility(View.VISIBLE);
	}
	
	protected class VideoInfoRequestListener implements RequestListener<YTVideoList>{

		@Override
		public void onRequestFailure(SpiceException arg0) {
			// TODO Auto-generated method stub
			Log.w(TAG, arg0.getMessage());//failure인 결과도 캐싱되나??TODO
			//끝까지 탐색한 경우 < request 200 OK json에 items가 안담겨있음, 자동 처리됨
			if(lvSearchResults.isRefreshing()){// 제한index를 넘어서 탐색한경우(youtube api가 500까지 제한),  < bad request 400 <--아무짓도 안하면 될듯
				lvSearchResults.onRefreshComplete();
			}
		}

		@Override
		public void onRequestSuccess(YTVideoList videos) {
			// TODO Auto-generated method stub
			if(listAdapter.getCount() == 0 && videos.videos.size() == 0)//total item이 0인경우... <request 200 OK... 이건 처리해줘야겠는데 결과가 없다고.
				showResultEmpty();
			else if(listAdapter.getCount() == 0){//첫 검색시
				showResultList();
			}
			listAdapter.addResults(videos.videos);
			if(lvSearchResults.isRefreshing()){
				lvSearchResults.onRefreshComplete();
			}
		}
	}
}
