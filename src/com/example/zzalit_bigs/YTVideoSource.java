package com.example.zzalit_bigs;
import android.util.Log;


public class YTVideoSource {
	private String sQaulity = null;	// Quality text from html source code
	private String sSTEREO3D = null;	// Format text from html source code
	private String sYTID = null;	// unique youtube video ID (11 alphanum letters) from html source code
	private String sITAG = null;	// unique ID format from html source code
	private String sTYPE = null;	// type  from html source code
	private String sUrl = null;		// URL to video of certian format (mpg,flv,webm,..?)
	private String sRESPART = null; // LD or 3D for filename
	private int sBps = 0; //bps는 해당 url의 헤더에 담겨있는 content-length와 YTVideo에 있는 duration을 통해 추산, 시간이 걸리는 작업(0.2초?)이므로
							//, 필요한 경우에만 메쏘드(쓰레드이용)를 통해 가져온다.
	
	private void constructor(String sURL, String sVideoURL) {
		try {
			this.sUrl = sURL; 
			String[] ssplittemp = sURL.split("&");

			this.sYTID = sVideoURL.substring( sVideoURL.indexOf("v=")+2);

			for (int i = 0; i < ssplittemp.length; i++) {
				if(ssplittemp[i].startsWith("itag")) this.sITAG = ssplittemp[i].substring( ssplittemp[i].indexOf('=')+1);
				if(this.sITAG != null) this.sQaulity = itagToQuality(this.sITAG);
				if(ssplittemp[i].startsWith("type")) this.sTYPE = ssplittemp[i].substring( ssplittemp[i].indexOf('=')+1);
				if(ssplittemp[i].startsWith("stereo3d")) this.sSTEREO3D = ssplittemp[i].substring( ssplittemp[i].indexOf('=')+1);
			}
			Log.d("YTU",String.format( "video url saved with key: %s - %s %s    - %s",this.getsITAG(), this.getsTYPE(), this.getsQuality(), this.getsUrl() ));
		} catch (NullPointerException npe) {
			// happens if URL for selection resolution could not be found
		}
	} // constructor
	
	private void constructor(String sURL, String sVideoURL,String sRESPART) {
		this.constructor(sURL, sVideoURL);
		this.sRESPART = sRESPART;
	} // constructor
	
	public YTVideoSource(String sURL, String sVideoURL) {
		super();
		this.constructor(sURL, sVideoURL);
	} // YTURL()
	
	public YTVideoSource(String sURL, String sVideoURL, String sRESPART) {
		this.constructor(sURL, sVideoURL,sRESPART);
	}

	/**
	 * @return the sQUALITY
	 */
	public String getsQuality() {
		return this.sQaulity;
	}

	/**
	 * @return the ssSTEREO3D
	 */
	public String getsSTEREO3D() {
		return this.sSTEREO3D;
	}

	/**
	 * @return the sYTID
	 */
	public String getsYTID() {
		return this.sYTID;
	}

	/**
	 * @return the sITAG
	 */
	public String getsITAG() {
		return this.sITAG;
	}

	/**
	 * @return the sURL
	 */
	public String getsUrl() {
		return this.sUrl;
	}
	
	/**
	 * @return the ssTYPE
	 */
	public String getsTYPE() {
		return this.sTYPE;
	}
	/**
	 * @return the sRESPART
	 */
	public String getsRESPART() {
		if (this.sRESPART==null)
			return "";
		else
			return this.sRESPART;
	}
	
	public int getsBps(){
		return this.sBps;
	}
	
	private String itagToQuality(String itag){
		String quality = "0";
		switch(Integer.parseInt(itag)){
		case 37:
		case 84:
		case 46:
		case 100:
		case 137:
			quality = "1080";
			break;
		case 22:
		case 45:
		case 136:
			quality = "720";
			break;
		case 35:
		case 135:
		case 44:
			quality = "480";
			break;
		case 18:
		case 34:
		case 82:
		case 43:
		case 134:
		case 102:
			quality = "360";
			break;
		case 133:
		case 5:
		case 36:
			quality = "240";
			break;
		case 160:
			quality = "144";
			break;
		case 17:
			quality = "114";
			break;
		case 140://audio
			quality = "-1";
			break;
		default ://unkwon itag
			Log.e("YTV","cant find matign quality from itag");
		}
		return quality;
	}

} // class YTURL
