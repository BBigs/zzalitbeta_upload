package com.example.zzalit_bigs;

import java.io.File;
import java.io.IOException;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Canvas;
import android.graphics.Matrix;
import android.media.MediaPlayer;
import android.media.MediaPlayer.OnSeekCompleteListener;
import android.media.MediaPlayer.OnVideoSizeChangedListener;
import android.util.AttributeSet;
import android.util.Log;
import android.view.SurfaceHolder;
import android.view.SurfaceView;
import android.view.View;
import android.view.ViewGroup;
import android.widget.RelativeLayout;


/**
 * serDataSource가 video파일과 프레임마다 뽑아낸 이미지리스트의 폴더를 둘다 받고있다. 이는 상당히 위험하다.
 * video파일만 넘기고 imagelist는 내부적으로 관리해야한다. 차후에 .ㅋ ㅎㅋㄱ흑ㅎ
 * 내부적으로 관리하는데에 있는 어려움은, 뷰마다 폴더경로를 따로해야한다. 또 jpegs를 뽑아내는 명령(file, time, fps등)이
 * 같다면 또 다른 걸로  관리, 같을경우에는 기존꺼 사용 등의 기능이 필요하다.
 * @author bigs
 *
 */
class LoopingVideoView extends RelativeLayout{

	public static final int FPS = 25;

	//play mode
	public static final int PLAY_MODE_NORMAL = 0;
	public static final int PLAY_MODE_REVERSE = 1;
	public static final int PLAY_MODE_PINGPONG = 2;

	//play speed
	public static final float PLAY_SPEED_MIN = 0.5f;
	public static final float PLAY_SPEED_MAX = 2.0f;
	public static final float PLAY_SPEED_DEFAULT = 1.0f;

	private MediaPlayer mp = null;
	private SurfaceView videoSurfaceView = null;
	private SurfaceView imageSurfaceView = null;
	private SurfaceHolder videoViewHolder = null;
	private SurfaceHolder imageViewHolder = null;
	private String videoUrl = null;
	private File tmpJpegsDir = null;
	private Thread playThread = null;
	private OnPreparedListener preparedListener = null;

	private boolean isDestroy = false;
	private boolean isPlaying = false;
	private boolean isPrepared = false;
//	private boolean isMpSeeking = false;
	private int pingpongDir = -1;// pingpongmode일 경우, play방향 : -1 역방향, 1 정방향 
	private int playMode = PLAY_MODE_NORMAL;
	private int dur = 0;
	private int currentTime = 0;
	private int startTime = 0;
	private int endTime = 0;
	private float playSpeed = PLAY_SPEED_DEFAULT;

	private Matrix imageMatrix = new Matrix();

	private int layoutWidth = -1;//초기 사이즈를 정하기 위한 변수
	private int videoWidth = -1;
	private int videoHeight = -1;
	private int layoutHeight = -1;

	public LoopingVideoView(Context context, AttributeSet attrs, int defStyle) {
		super(context, attrs, defStyle);
		init();
		// TODO Auto-generated constructor stub
	}

	public LoopingVideoView(Context context, AttributeSet attrs) {
		super(context, attrs);
		init();
		// TODO Auto-generated constructor stub
	}

	public LoopingVideoView(Context context) {
		super(context);
		init();
		// TODO Auto-generated constructor stub
	}
	private void init(){
		mp = new MediaPlayer();
		videoSurfaceView = new SurfaceView(getContext());
		videoSurfaceView.setZOrderOnTop(true);
		imageSurfaceView = new SurfaceView(getContext());
		imageSurfaceView.setZOrderOnTop(true);
		videoViewHolder = videoSurfaceView.getHolder();
		imageViewHolder = imageSurfaceView.getHolder();
		imageSurfaceView.setVisibility(View.INVISIBLE);
		videoViewHolder.addCallback(new SurfaceHolder.Callback() {
			@Override
			public void surfaceChanged(SurfaceHolder holder, int format, int width,
					int height) {
			}

			@Override
			public void surfaceCreated(SurfaceHolder holder) {
				mp.setDisplay(videoViewHolder);
				if(isPrepared)
					start();
			}

			@Override
			public void surfaceDestroyed(SurfaceHolder holder) {
				if(mp == null)
					return;
				mp.setDisplay(null);
				if(mp.isPlaying())
					pause();//mp.pause();
			}
		});
		imageViewHolder.addCallback(new SurfaceHolder.Callback() {

			@Override
			public void surfaceDestroyed(SurfaceHolder holder) {
				//				Toast.makeText(getContext(), "surface destroyed", Toast.LENGTH_SHORT).show();
			}

			@Override
			public void surfaceCreated(SurfaceHolder holder) {
				//				Toast.makeText(getContext(), "surface created", Toast.LENGTH_SHORT).show();
			}

			@Override
			public void surfaceChanged(SurfaceHolder holder, int format, int width,
					int height) {
			}
		});
		mp.setOnVideoSizeChangedListener(new OnVideoSizeChangedListener() {

			@Override
			public void onVideoSizeChanged(MediaPlayer mp, int width, int height) {
				// TODO Auto-generated method stub

			}
		});
//		mp.setOnSeekCompleteListener(new OnSeekCompleteListener() {
//			
//			@Override
//			public void onSeekComplete(MediaPlayer mp) {
//				// TODO Auto-generated method stub
//				isMpSeeking = false;
//			}
//		});
		mp.setOnPreparedListener(new MediaPlayer.OnPreparedListener() {

			@Override
			public void onPrepared(MediaPlayer mp) {
				mp.setLooping(true);
				endTime = dur = mp.getDuration();
				videoHeight = mp.getVideoHeight();
				videoWidth = mp.getVideoWidth();
				adjustSurfaceParams();
				isPrepared = true;
				if(preparedListener != null)
					preparedListener.onPrepared(mp);
			}
		});
		tmpJpegsDir = getContext().getDir("LoopingVideoView", Context.MODE_PRIVATE);
		File[] files = tmpJpegsDir.listFiles();
		for(File file : files)
			file.delete();

		playThread = new PlayThread();
		playThread.start();

		RelativeLayout.LayoutParams params = new RelativeLayout.LayoutParams(RelativeLayout.LayoutParams.MATCH_PARENT, RelativeLayout.LayoutParams.MATCH_PARENT);
		RelativeLayout.LayoutParams params1 = new RelativeLayout.LayoutParams(RelativeLayout.LayoutParams.MATCH_PARENT, RelativeLayout.LayoutParams.MATCH_PARENT);
		params.addRule(RelativeLayout.CENTER_IN_PARENT);
		params1.addRule(RelativeLayout.CENTER_IN_PARENT);
		this.addView(videoSurfaceView, params);
		this.addView(imageSurfaceView, params1);
		videoSurfaceView.setVisibility(View.INVISIBLE);
		imageSurfaceView.setVisibility(View.INVISIBLE);
	}
	public void setDataSource(String videoPath, String jpegsPath) throws IllegalArgumentException, SecurityException, IllegalStateException, IOException{
		mp.setDataSource(videoPath);
		videoUrl = videoPath;
		tmpJpegsDir = new File(jpegsPath);
	}
	public void prepare() throws IllegalStateException, IOException{
		mp.prepare();
	}
	public void prepareAsync(){
		mp.prepareAsync();
	}

	public void setOnPreparedListener(OnPreparedListener listener){
		preparedListener = listener;
	}

	public void start(){
		isPlaying = true;
		if(playMode == PLAY_MODE_NORMAL && playSpeed == PLAY_SPEED_DEFAULT){
			imageSurfaceView.setVisibility(View.INVISIBLE);
		}else{
			imageSurfaceView.setVisibility(View.VISIBLE);
		}
	}
	public void start(int playMode){
		setMode(playMode);
		start();
//		if(playThread.getState() == Thread.State.WAITING){
//			synchronized (playThread) {
//				playThread.notify();
//			}
//		}
	}
	public void pause(){
		isPlaying = false;
		imageSurfaceView.setVisibility(View.VISIBLE);
	}
	public void setLoopingTime(int startTime, int endTime){
		this.startTime = startTime;
		this.endTime = endTime;
	}
	public boolean isPlaying(){
		return isPlaying;
	}
	public void seekTo(int timeInMilles){
		currentTime = timeInMilles;
		mp.seekTo(currentTime);
	}
	public void setOnSeekCompleteListener(OnSeekCompleteListener listener){
		if(mp != null)
			mp.setOnSeekCompleteListener(listener);
	}
	
	public int getCurrentTime(){
		return currentTime;
	}
	public void setMode(int mode){//함수내 명령문들의 순서를 바꿀경우 동작하지 않을수있다.(쓰레딩 동기화를 따로 안하고, 명령순서로 정함)
		if(mode == -1)
			return;
		if(mode == PLAY_MODE_NORMAL && playSpeed == PLAY_SPEED_DEFAULT){
			imageSurfaceView.setVisibility(View.INVISIBLE);
		}else{
			imageSurfaceView.setVisibility(View.VISIBLE);
		}
		playMode = mode;
		if(mp != null){
			if(mp.isPlaying())
				mp.pause();
			switch(playMode){
			case PLAY_MODE_NORMAL:
			case PLAY_MODE_PINGPONG:
				seekTo(startTime);
				break;
			case PLAY_MODE_REVERSE:
				currentTime = endTime;
				break;
			}
		}

	}
	public int getMode(){
		return playMode;
	}
	public void setPlaySpeed(float rate){//함수내 명령문들의 순서를 바꿀경우 동작하지 않을수있다.(쓰레딩 동기화를 따로 안하고, 명령순서로 정함)
		if(playMode == PLAY_MODE_NORMAL && rate == PLAY_SPEED_DEFAULT){
			imageSurfaceView.setVisibility(View.INVISIBLE);
		}else{
			imageSurfaceView.setVisibility(View.VISIBLE);
		}
//		if(mp != null){
//			if(mp.isPlaying())
//				mp.pause();
//			switch(playMode){
//			case PLAY_MODE_NORMAL:
//			case PLAY_MODE_PINGPONG:
//				seekTo(0);
//				break;
//			case PLAY_MODE_REVERSE:
//				currentTime = dur;
//				break;
//			}
//		}
		playSpeed = rate;
	}
	public float getPlaySpeed(){
		return playSpeed;
	}
	public void setImageSurfaceVisibility(int visibility){
		imageSurfaceView.setVisibility(visibility);
	}

	public void recycle(){
		if(mp == null)
			return;
		File[] files = tmpJpegsDir.listFiles();
		for(File file : files)
			file.delete();
		isDestroy = true;
		if(!isPlaying)
			start();
		try {
			playThread.join();
		} catch (InterruptedException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		mp.release();
		mp = null;
	}

	private boolean isSurfaceSized = false;
	private void adjustSurfaceParams(){
		if(isSurfaceSized)
			return;
		if( layoutHeight != -1 && videoHeight != -1){
			isSurfaceSized = true;
			float scale = Math.min((float)layoutWidth/videoWidth, (float)layoutHeight/videoHeight); 
			ViewGroup.LayoutParams params = videoSurfaceView.getLayoutParams();
			ViewGroup.LayoutParams params1 = imageSurfaceView.getLayoutParams();
			params1.width = params.width = (int) (videoWidth*scale);
			params1.height = params.height = (int) (videoHeight*scale);
			imageMatrix.setScale(scale, scale);
			videoSurfaceView.setVisibility(View.VISIBLE);
			imageSurfaceView.setVisibility(View.VISIBLE);
			Log.d("bigs"," adjst surface scale:"+scale+" "+layoutWidth+" "+videoWidth+" "+layoutHeight+" "+videoHeight);
			requestLayout();
		}
	}

	@Override
	protected void onSizeChanged(int w, int h, int oldw, int oldh) {
		// TODO Auto-generated method stub
		super.onSizeChanged(w, h, oldw, oldh);
		layoutHeight = h;
		layoutWidth = w;
		adjustSurfaceParams();
	}
	public interface OnPreparedListener{
		public void onPrepared(MediaPlayer mp);
	}

	private class PlayThread extends Thread{

		@Override
		public void run() {
			// TODO Auto-generated method stub
			while(!isDestroy){
				if(mp == null){
					try {
						Thread.sleep(1000/FPS);
					} catch (InterruptedException e) {
						// TODO Auto-generated catch block
						e.printStackTrace();
					}
					continue;
				}
				if(imageSurfaceView.getVisibility() == View.INVISIBLE){
					if(isPlaying){
						if(mp.getCurrentPosition() > endTime)
							mp.seekTo(startTime);
						if(!mp.isPlaying())
							mp.start();
						currentTime = mp.getCurrentPosition();
					}
					try {
						Thread.sleep(1000/FPS);
					} catch (InterruptedException e) {
						// TODO Auto-generated catch block
						e.printStackTrace();
					}
				}
				else{
					if(mp.isPlaying())
						mp.pause();
					long preTime= System.currentTimeMillis();
					if(imageViewHolder != null){
						Canvas canvas = imageViewHolder.lockCanvas();
						if(canvas != null){
							if(isPlaying)
								currentTime = getNextTime();//동기화를 위해 선언문의 순서를  요로코롬
							int nexVideoTime = currentTime;
							Bitmap bitmap = BitmapFactory.decodeFile(tmpJpegsDir+"/"+getImageIndex(nexVideoTime)+".jpeg");
							if(bitmap != null){
								canvas.drawBitmap(bitmap, imageMatrix, null);
								bitmap.recycle();
							}
							imageViewHolder.unlockCanvasAndPost(canvas);
						}
					}
					long elapsedTime = System.currentTimeMillis() - preTime;
					long idleTime = 1000/FPS - elapsedTime;
					try {
						Log.d("bigs","play thread idleTime : "+idleTime);
						if(idleTime > 0)
							Thread.sleep(idleTime);
					} catch (InterruptedException e) {
						// TODO Auto-generated catch block
						e.printStackTrace();
					}
				}

			}
//			else{
//				synchronized (playThread) {
//					try {
//						if(mp.isPlaying())
//							mp.pause();
//						playThread.wait();//play가 아닐경우 대기
//					} catch (InterruptedException e) {
//						// TODO Auto-generated catch block
//						e.printStackTrace();
//					}	
//				}
//			}
		}
	}
	public void destroy(){
		isDestroy = true;
	}

	private int getNextTime(){
		int nextTime = currentTime;
		switch(playMode){
		case PLAY_MODE_NORMAL:
			nextTime += 1000/FPS * playSpeed;
			if(nextTime > endTime)
				nextTime = nextTime - endTime + startTime;
			break;
		case PLAY_MODE_REVERSE:
			nextTime -= 1000/FPS * playSpeed;
			if(nextTime < startTime)
				nextTime = endTime + (startTime-nextTime);
			break;
		case PLAY_MODE_PINGPONG:
			nextTime += pingpongDir * 1000/FPS * playSpeed;
			if(nextTime < startTime){
				nextTime = startTime - (nextTime-startTime);
				pingpongDir = 1;
			}
			else if(nextTime > endTime){
				nextTime = endTime-(nextTime-endTime);
				pingpongDir = -1;
			}
			break;
		}
		return nextTime;
	}
	private int getImageIndex(int time){
		return (int) ((float)time/1000*FPS)+(time != dur?1:0);
	}
}

