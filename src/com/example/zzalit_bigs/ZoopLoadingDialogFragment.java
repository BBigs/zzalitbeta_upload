package com.example.zzalit_bigs;

import android.app.Dialog;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.graphics.drawable.Drawable;
import android.os.Bundle;
import android.support.v4.app.DialogFragment;
import android.view.Gravity;
import android.view.View;
import android.view.ViewGroup;
import android.view.ViewParent;
import android.view.Window;
import android.view.WindowManager;
import android.view.animation.AccelerateDecelerateInterpolator;
import android.view.animation.Animation;
import android.view.animation.LinearInterpolator;
import android.view.animation.RotateAnimation;
import android.widget.Button;
import android.widget.FrameLayout.LayoutParams;
import android.widget.ImageView;
import android.widget.RelativeLayout;

public class ZoopLoadingDialogFragment extends DialogFragment{
	public static final int RATE_CENTER_SPACE_WIDTH = 18;

	private static final int SPACE = 3000;

	private Drawable rotateingImageDrawable = null;
	private Drawable backgroundDrawalbe = null;
	private int width = 0;
	private int height = 0;
	private int rotatingImageWidth = 0;
	private int rotatingImageHeight = 0;
	private int centerMargin = 0;
	private int theme = android.R.style.Theme;

	private ViewGroup rootView = null;
	
	public static ZoopLoadingDialogFragment newInstance(int width, int height,
			Drawable background, Drawable rotatingImage, int imageWpx, int imageHpx){
		ZoopLoadingDialogFragment instance = new ZoopLoadingDialogFragment();
		instance.setBackgroundDrarable(background);
		instance.setRotatingImageDrawable(rotatingImage, imageWpx, imageHpx);
		instance.setDialogSize(width, height);
		return instance;
	}
	public static ZoopLoadingDialogFragment newInstance(int width, int height,
			Drawable background, Drawable rotatingImage, int imageWpx, int imageHpx, int theme){
		ZoopLoadingDialogFragment instance = newInstance(width, height, background, rotatingImage, imageWpx, imageHpx);
		instance.theme = theme;
		return instance;
	}

	@Override
	public Dialog onCreateDialog(Bundle savedInstanceState) {
		// TODO Auto-generated method stub

		rootView = new RelativeLayout(getActivity());
		
		WindowManager.LayoutParams rootParams = new WindowManager.LayoutParams(width, height);
		rootParams.gravity = Gravity.CENTER;
		rootView.setLayoutParams(rootParams);
		rootView.setBackgroundDrawable(backgroundDrawalbe);
		
		ImageView leftRotatingImageView = new ImageView(getActivity());
		ImageView rightRotatingImageView = new ImageView(getActivity());
		View space = new View(getActivity());
		space.setId(SPACE);

		RelativeLayout.LayoutParams spaceParams = new RelativeLayout.LayoutParams(centerMargin, 20);
		spaceParams.addRule(RelativeLayout.CENTER_IN_PARENT);
		space.setLayoutParams(spaceParams);
		
		RotateAnimation leftAnim = new RotateAnimation(0,360,Animation.RELATIVE_TO_SELF, 0.5f,Animation.RELATIVE_TO_SELF, 0.5f);
		RotateAnimation rightAnim = new RotateAnimation(0,360,Animation.RELATIVE_TO_SELF, 0.5f,Animation.RELATIVE_TO_SELF, 0.5f);
		leftAnim.setDuration(1000);
		leftAnim.setRepeatCount(Animation.INFINITE);
		leftAnim.setInterpolator(new LinearInterpolator());
		rightAnim.setDuration(1000);
		rightAnim.setRepeatCount(Animation.INFINITE);
		rightAnim.setInterpolator(new LinearInterpolator());

		leftRotatingImageView.setImageDrawable(rotateingImageDrawable);
		leftRotatingImageView.startAnimation(leftAnim);

		rightRotatingImageView.setImageDrawable(rotateingImageDrawable);
		rightRotatingImageView.startAnimation(rightAnim);
		
		RelativeLayout.LayoutParams leftImageParams = new RelativeLayout.LayoutParams(rotatingImageWidth, rotatingImageHeight);
		RelativeLayout.LayoutParams rightImageParams = new RelativeLayout.LayoutParams(rotatingImageWidth, rotatingImageHeight);
		
		leftImageParams.addRule(RelativeLayout.LEFT_OF, space.getId());
		leftImageParams.addRule(RelativeLayout.CENTER_VERTICAL);
		rightImageParams.addRule(RelativeLayout.RIGHT_OF, space.getId());
		rightImageParams.addRule(RelativeLayout.CENTER_VERTICAL);
		leftRotatingImageView.setLayoutParams(leftImageParams);
		rightRotatingImageView.setLayoutParams(rightImageParams);

		rootView.addView(space);
		rootView.addView(leftRotatingImageView);
		rootView.addView(rightRotatingImageView);

		Dialog loadingDialog = new Dialog(getActivity(), theme);
		loadingDialog.getWindow().requestFeature(Window.FEATURE_NO_TITLE);
		loadingDialog.setContentView(rootView);
		Window window = loadingDialog.getWindow();
		window.setLayout(width, height);
		window.setGravity(Gravity.CENTER);
		window.setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
		return loadingDialog;
	}
	
	@Override
	public void onResume() {
		// TODO Auto-generated method stub
		super.onResume();
	}

	
	public void setRotatingImageDrawable(Drawable drawable, int widthPx, int heightPx){
		rotateingImageDrawable = drawable;
		rotatingImageWidth = widthPx;
		rotatingImageHeight = heightPx;
	}
	public void setBackgroundDrarable(Drawable drawable){
		backgroundDrawalbe = drawable;
	}

	public void setDialogSize(int width, int height){
		this.width = width;
		this.height = height;
		centerMargin = (int) ((float)width/RATE_CENTER_SPACE_WIDTH);
	}


}
