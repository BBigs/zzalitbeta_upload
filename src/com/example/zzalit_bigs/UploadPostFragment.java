package com.example.zzalit_bigs;

import android.media.MediaPlayer;
import android.media.MediaPlayer.OnPreparedListener;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.ScrollView;
import android.widget.VideoView;

import com.actionbarsherlock.app.SherlockFragment;
import com.actionbarsherlock.view.Menu;
import com.actionbarsherlock.view.MenuInflater;
import com.actionbarsherlock.view.MenuItem;

public class UploadPostFragment extends SherlockFragment{
	public static final String TAG = "UploadPostFragment";
	public static final int ITEM_POST_COMPLETE = 2;

	static public UploadPostFragment newInstance(String url){
		UploadPostFragment fragment = new UploadPostFragment();
		Bundle bundle = new Bundle();
		bundle.putString("url", url);
		fragment.setArguments(bundle);
		return fragment;
	}
	
	private ScrollView rootView = null;
	private VideoView videoView = null;

	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container,
			Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		rootView = (ScrollView)inflater.inflate(R.layout.fragment_upload_post, null);
		videoView = (VideoView)rootView.findViewById(R.id.video_result_view);
		videoView.setZOrderMediaOverlay(true);
		videoView.setOnPreparedListener(new OnPreparedListener() {
			
			@Override
			public void onPrepared(MediaPlayer mp) {
				// TODO Auto-generated method stub
				mp.setLooping(true);
				mp.start();
			}
		});
		
		return rootView;
	}
	
	@Override
	public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
		// TODO Auto-generated method stub
		menu.clear();
		MenuItem itemComplete = menu.add(Menu.NONE, ITEM_POST_COMPLETE, Menu.NONE, "post_complete");
		itemComplete.setIcon(R.drawable.upload_video_complete_button);
		itemComplete.setShowAsAction(MenuItem.SHOW_AS_ACTION_ALWAYS);
	}
	
	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		// TODO Auto-generated method stub
		switch (item.getItemId()) {
		case ITEM_POST_COMPLETE:
			break;
		}
		return true;
	}
}
