package com.example.zzalit_bigs;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.Bitmap.Config;
import android.graphics.BitmapShader;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Matrix;
import android.graphics.Paint;
import android.graphics.Rect;
import android.graphics.Shader.TileMode;
import android.graphics.drawable.BitmapDrawable;
import android.graphics.drawable.Drawable;
import android.graphics.drawable.GradientDrawable;
import android.graphics.drawable.GradientDrawable.Orientation;
import android.os.Handler;
import android.os.Looper;
import android.util.AttributeSet;
import android.util.Log;
import android.view.GestureDetector;
import android.view.GestureDetector.OnGestureListener;
import android.view.MotionEvent;
import android.view.WindowManager;
import android.widget.OverScroller;

public class ThumbnailSeekBar extends VideoThumbGeneratingListView implements OnGestureListener{

	private GestureDetector gdt = null;
	private Belt belt = null;
	private OverScroller scroller = null;
	private OnSeekListener seekListener = null;
	private BeltListener beltListener = null;
	private Handler uiHandler = null;
	private boolean isBeltResizing = false;
	private boolean isBeltHighlighted = false;
	private Bitmap seekBarImage = null;
	private Paint pntBeltHighlight = null;
	private int seekBarPos = 0;
	//	private long currentTime  = 0 ;
	//	private LinkedHashMap<Integer, TouchPointer> touchMap = null;

	public ThumbnailSeekBar(Context context, AttributeSet attrs, int defStyle) {
		super(context, attrs, defStyle);
		init();
		// TODO Auto-generated constructor stub
	}

	public ThumbnailSeekBar(Context context, AttributeSet attrs) {
		super(context, attrs);
		init();
		// TODO Auto-generated constructor stub
	}

	public ThumbnailSeekBar(Context context) {
		super(context);
		init();
		// TODO Auto-generated constructor stub
	}

	private void init(){
		gdt = new GestureDetector(getContext(), this);
		scroller = new OverScroller(getContext());
		uiHandler = new Handler(Looper.getMainLooper());
		pntBeltHighlight = new Paint();
		pntBeltHighlight.setColor(0x4c000000);
		//		touchMap = new LinkedHashMap<Integer, TouchPointer>();
	}

	public void setOnSeekListener(OnSeekListener listener){
		seekListener = listener;
	}
	public void setSeekBarImage(Bitmap bitmap, int width, int height){
		seekBarImage = Bitmap.createScaledBitmap(bitmap, width, height, false);
	}
	public void setBeltListener(BeltListener listener){
		beltListener = listener;
	}
	public void setBelt(int beltWidth){
		belt = new Belt(new Rect(0, 0, beltWidth, 0));
	}
	/**
	 * setBelt()가 선행 되어야함
	 * @param transformable
	 * @param transformableEdgeSize
	 */
	public Belt enableBeltTransform(int minTime, int maxTime, int transformableEdgeSize){
		if(belt != null)
			belt.enableTransform(timeToX(minTime), timeToX(maxTime), transformableEdgeSize);
		return belt;
	}
	public Belt getBelt(){
		return belt;
	}
	public void disableBeltTransform(){
		if(belt != null)
			belt.disableTransform();
	}
	public boolean isBeltTransformable(){
		if(belt != null)
			return belt.transformable;
		else
			return false;
	}
	public void setBeltHighlist(boolean flag){
		this.isBeltHighlighted = flag;
	}
	/**
	 * uiThread에서만 문제없이 돌아감 
	 * @return
	 */
	public long getCurrentTime(){
		//		Log.d("bigs", "getCurrentTime to "+xToTime((getScrolledX()+belt.getLeft()))+", x:"+(getScrolledX()+belt.getLeft()));
		return xToTime((getScrolledX()+belt.getLeft()));
	}
	/**
	 * uiThread에서만 문제없이 돌아감
	 * 
	 * @param time
	 * @param duration smooth하게 그시간으로 옮겨지는데.. 거기에 걸리는 scroll시간
	 */
	public synchronized void setCurrentTime(long time, int duration){
		if(time > videoTotalTime)
			time = videoTotalTime;
		int x = timeToX(time);
		int distance = x - timeToX(getCurrentTime());
		if( listWidth <= getWidth() ){//전체리스트의 크기가 화면너비보다 작을경우, 그냥 밸트만 슝슝 움직임
			smoothScrollBy(-getScrolledX(), 1);
			belt.setCenterX((int) Math.min(x+belt.width()/2, listWidth-belt.width()/2));
		}else if(x < getWidth()/2-belt.width()/2){//리스트의 처음일경우, 
			smoothScrollBy(-getScrolledX(), 1);
			belt.setCenterX(x+belt.width()/2);
		}else if(x+belt.width()/2 > listWidth-getWidth()/2 && listWidth-getWidth() <= getScrolledX()){//리스트 맨마지막에서 화면의 반을 넘어갔을 경우
			belt.setCenterX((int) Math.min((x-(listWidth-getWidth())+belt.width()/2), getWidth()-belt.width()/2));
		}else{
			belt.setCenterX(getWidth()/2);	
			smoothScrollBy(distance, duration);
		}
		invalidate();
	}
	public int getScrolledX(){
		if(getChildCount() ==  0)
			return 0;
		return (getFirstVisiblePosition()+1)*thumbWidth - getChildAt(0).getRight();
	}
	public boolean isBeltResizing(){
		return isBeltResizing;
	}
	@Override
	protected void onSeekingStateChanged(boolean isSeeking){
		super.onSeekingStateChanged(isSeeking);
		if(!isSeeking){
			if(seekListener != null){
				uiHandler.post(new Runnable() {
					@Override
					public void run() {
						seekListener.onSeeked(getCurrentTime());
						ThumbnailSeekBar.this.isSeeking = false;
					}
				});
			}
		}else{
			ThumbnailSeekBar.this.isSeeking = true;
		}
	}

	@Override
	protected void onScrollChanged(int l, int t, int oldl, int oldt) {
		// TODO Auto-generated method stub
		super.onScrollChanged(l, t, oldl, oldt);

	}

	public static final int TOUCH_MODE_SCROLL = 0;
	public static final int TOUCH_MODE_BELT_LEFT = 1;
	public static final int TOUCH_MODE_BELT_RIGHT = 2;

	float preX  = -300000;
	int touchMode = TOUCH_MODE_SCROLL;
	int distLeftEdgeToTouch = 0;
	int distRightEdgeToTouch = 0;
	@Override
	public boolean onTouchEvent(MotionEvent ev) {
		// TODO Auto-generated method stub
		if(belt == null)
			return super.onTouchEvent(ev);

		int beltWidth = belt.width();
		boolean result = false;
		//		}
		float x = ev.getX();
		float dx = x-preX;
		switch(ev.getAction() & MotionEvent.ACTION_MASK){
		case MotionEvent.ACTION_DOWN:
		case MotionEvent.ACTION_POINTER_DOWN:
			x = ev.getX(ev.getActionIndex());
			if(belt.isTransformable()){
				if(belt.containInLeftEdge((int) x)){
					touchMode |= TOUCH_MODE_BELT_LEFT;
					distLeftEdgeToTouch = (int) (x - belt.getLeft());
					isBeltResizing = true;
				}
				if(belt.containInRightEdge((int)x)){
					touchMode |= TOUCH_MODE_BELT_RIGHT;
					distRightEdgeToTouch = (int) (belt.getRight()-x);
					isBeltResizing = true;
				}
			}
			result = true;
			break;
		case MotionEvent.ACTION_UP:
			if((touchMode & TOUCH_MODE_BELT_LEFT) > 0 || (touchMode & TOUCH_MODE_BELT_RIGHT) > 0){
				isBeltResizing = false;
				if(beltListener != null){
					uiHandler.post(new Runnable() {
						@Override
						public void run() {
							// TODO Auto-generated method stub
							beltListener.onBeltResized(xToTime(getScrolledX()+belt.getLeft()), xToTime(getScrolledX()+belt.getRight()));
						}
					});
				}
			}
			touchMode = TOUCH_MODE_SCROLL;
			break;
		case MotionEvent.ACTION_POINTER_UP:
			if(ev.getPointerCount() == 2){
				if(ev.getX(ev.getActionIndex()) < belt.centerX())
					touchMode = TOUCH_MODE_BELT_RIGHT;
				else
					touchMode = TOUCH_MODE_BELT_LEFT;
			}
			break;
		case MotionEvent.ACTION_MOVE:
			if(touchMode == (TOUCH_MODE_BELT_LEFT | TOUCH_MODE_BELT_RIGHT)){
				if(ev.getPointerCount() == 2 ){
					belt.setLeftRight((int) Math.min(ev.getX(0), ev.getX(1))-distLeftEdgeToTouch, (int) Math.max(ev.getX(0), ev.getX(1))+distRightEdgeToTouch);
					result = true;
				}
			}else if(touchMode == TOUCH_MODE_BELT_LEFT){
				belt.setLeftRight((int) x-distLeftEdgeToTouch, belt.getRight());
				result = true;
			}else if(touchMode == TOUCH_MODE_BELT_RIGHT){
				belt.setLeftRight(belt.getLeft(), (int)x+distRightEdgeToTouch);
				result = true;
			}
			else if(touchMode == TOUCH_MODE_SCROLL){
				//				Log.d("TouchMode","scroll!");
				if(x-preX > getWidth()/2)//급격한 변화는 무시
					return true;
				belt.setCenterX(belt.centerX() - (int)(dx>0 ? Math.ceil(dx): -Math.ceil(-dx)));
				if(getFirstVisiblePosition() == 0 &&getChildAt(0).getLeft() == getLeft()){//list가 처음일때
					if(belt.centerX() <= beltWidth/2){
						belt.setCenterX(beltWidth/2);
					}
					//반이하일때                                          
					if(belt.centerX() <= getWidth()/2)
						result= true; 
				}
				if(getLastVisiblePosition() == getAdapter().getCount()-1 && getChildAt(getChildCount()-1).getRight() <= getRight()){//list의 마지막까지 왔는지
					if(belt.centerX() >= Math.min(getChildAt(getChildCount()-1).getRight(), getWidth())-beltWidth/2){
						belt.setCenterX(Math.min(getChildAt(getChildCount()-1).getRight(), getWidth())-beltWidth/2);
					}
					if(belt.centerX() >= getWidth()/2)
						result = true;
				}
				if(!result)
					belt.setLeftRight(getWidth()/2 - beltWidth/2, getWidth()/2 + beltWidth/2);

				if(seekListener != null){
					uiHandler.post(new Runnable() {
						@Override
						public void run() {
							seekListener.onSeeking(getCurrentTime());
						}
					});
				}
			}
			break;
		}
		preX = x;
		gdt.onTouchEvent(ev);
		//		Log.d("bigs","touch getScrolled : "+getScrolledX());
		if(result){
			invalidate();
			//			Log.d("test","ThumbnailSeekBar onTouch result "+result+" action:"+ev.getAction());
			ev.setAction(MotionEvent.ACTION_DOWN);//scroll시 down, move, up을 처리하여 동작하는데, down을 초기화시켜줌으로써, 스크롤시작할때 화아아악 움직이는 것을 방지
			super.onTouchEvent(ev);
			return true;
		}
		//		Log.d("test","ThumbnailSeekBar onTouch result "+result+" action:"+ev.getAction());
		return super.onTouchEvent(ev);
	}
	
	public void setSeekBarPos(int pos){
		seekBarPos = pos;
		invalidate();
	}

	@Override
	protected void dispatchDraw(Canvas canvas) {
		// TODO Auto-generated method stub
		super.dispatchDraw(canvas);
		if(isBeltHighlighted){
			canvas.drawRect(0, belt.rect.top, belt.getLeft(), belt.rect.bottom, pntBeltHighlight);
			canvas.drawRect(belt.getRight(), belt.rect.top, getRight(), belt.rect.bottom, pntBeltHighlight);
		}
		if(seekBarImage != null && !isSeeking && !isBeltResizing){//belt seek bar draw{
			if(seekBarPos + seekBarImage.getWidth() < belt.getRight())
				canvas.drawBitmap(seekBarImage, seekBarPos, getTop(), null);
		}
		if(belt != null){
			belt.rect.bottom = getBottom();
			belt.draw(canvas);
		}
	}
	@Override
	public boolean onDown(MotionEvent e) {
		// TODO Auto-generated method stub
		scroller.forceFinished(true);
		return false;
	}
	@Override
	public boolean onFling(MotionEvent e1, MotionEvent e2, float velocityX,
			float velocityY) {
		// TODO Auto-generated method stub
		//		Rect preRect = new Rect(belt.rect);
		if(getFirstVisiblePosition() == 0 &&getChildAt(0).getLeft() == getLeft()){//리스트의 첫부분일때
			int maxX = getWidth()/2;
			if(getLastVisiblePosition() == getAdapter().getCount()-1 && getChildAt(getChildCount()-1).getRight() <= getRight())
				maxX = getChildAt(getChildCount()-1).getRight()-belt.width()/2;
			scroller.fling(belt.centerX(), 0, -(int)velocityX, 0,
					belt.width()/2, maxX, 0, 0);

		}else if(getLastVisiblePosition() == getAdapter().getCount()-1 && getChildAt(getChildCount()-1).getRight() <= getRight()){//list의 마지막까지 왔는지
			scroller.fling(belt.centerX(), 0, -(int)velocityX, 0,
					getWidth()/2, getWidth()-belt.width()/2,0,0);
		}
		//		Log.d("bigs","fling min:"+(-(getWidth()/2-preRect.centerX()))+", max:"+preRect.centerX());
		invalidate();
		return false;
	}
	@Override
	public void computeScroll() {
		// TODO Auto-generated method stub
		super.computeScroll();
		if(scroller.computeScrollOffset()){
			int x = scroller.getCurrX();
			Log.d("bigs","compute scroll x : "+x);
			belt.setCenterX(x);
			//			setCurrentTime(xToTime(getScrolledX()+belt.getLeft()),1);
			if(!isSeeking){
				onSeekingStateChanged(true);
			}
			if(scroller.getCurrX() == scroller.getFinalX()){
				Log.d("bigs", "computeScroll compelete");
				scroller.forceFinished(true);
				if(seekListener != null){
					uiHandler.post(new Runnable() {
						@Override
						public void run() {
							if(isSeeking){
								onSeekingStateChanged(false);
							}
						}
					});
				}
			}else{
				invalidate();
			}
		}
	}
	@Override
	public void onLongPress(MotionEvent e) {
		// TODO Auto-generated method stub
	}

	@Override
	public boolean onScroll(MotionEvent e1, MotionEvent e2, float distanceX,
			float distanceY) {
		// TODO Auto-generated method stub
		return false;
	}

	@Override
	public void onShowPress(MotionEvent e) {
		// TODO Auto-generated method stub

	}

	@Override
	public boolean onSingleTapUp(MotionEvent e) {
		// TODO Auto-generated method stub
		return false;
	}

	protected static Bitmap drawableToBitmap (Drawable drawable) {
		if (drawable instanceof BitmapDrawable) {
			return ((BitmapDrawable)drawable).getBitmap();
		}

		Bitmap bitmap = Bitmap.createBitmap(drawable.getIntrinsicWidth(), drawable.getIntrinsicHeight(), Config.ARGB_8888);
		Canvas canvas = new Canvas(bitmap); 
		drawable.setBounds(0, 0, canvas.getWidth(), canvas.getHeight());
		drawable.draw(canvas);

		return bitmap;
	}

	public class Belt{
		private Rect rect = null;
		private Bitmap backgroundImage = null;
		private Bitmap topImage = null;
		private Bitmap leftImage = null;
		private Bitmap rightImage = null;
		private Bitmap bottomImage = null;
		private Rect topRect = null;
		private Rect topSrcRect = null;
		private Rect bottomRect = null;
		private Rect leftRect = null;
		private Rect rightRect = null;
		private int shadowWidth = 0;
		private GradientDrawable leftShadow = null;//left, right, bottom은 그림자의 방향
		private GradientDrawable rightShadow = null;
		private GradientDrawable bottomShadow = null;
		private boolean bShadow = false;
		private Paint pntTopImage = null;
		private int minSize = 0;
		private int maxSize = 0;
		private int transformableEdgeSize = 0;
		private boolean transformable = false;
		public Belt(Rect rect){
			this.rect = rect;
			this.minSize = 0;
			this.maxSize = rect.right;
			pntTopImage = new Paint();
		}
		public void setBackgroundImage(Bitmap image){
			this.backgroundImage = image;
		}
		public void setTopImage(Bitmap image, int height){
			WindowManager wm = (WindowManager)getContext().getSystemService(Context.WINDOW_SERVICE);
			image = Bitmap.createScaledBitmap(image,(int)( height* (image.getWidth()/(float)image.getHeight())), height, false);
			this.topImage = Bitmap.createBitmap(wm.getDefaultDisplay().getWidth(), image.getHeight(), Bitmap.Config.ARGB_8888);
			pntTopImage.setShader(new BitmapShader(image, TileMode.REPEAT, TileMode.REPEAT));
			Canvas canvas = new Canvas(this.topImage);
			canvas.drawRect(0, 0, wm.getDefaultDisplay().getWidth(), height, pntTopImage);
			this.topRect = new Rect(0, 0, 0, height);
			this.topSrcRect = new Rect(topRect);
		}
		public void setLeftImage(Bitmap image, int width){
			this.leftImage = image;
			this.leftRect = new Rect(0, 0, width, 0);
		}
		public void setRightImage(Bitmap image, int width){
			this.rightImage = image;
			this.rightRect = new Rect(0, 0, width, 0);
		}
		public void setBottomImage(Bitmap image, int height){
			this.bottomImage = image;
			this.bottomRect = new Rect(0, 0, 0, height);
		}
		public void setShadow(int shadowWidth){
			this.bShadow = true;
			this.shadowWidth = shadowWidth;
			leftShadow = new GradientDrawable(Orientation.RIGHT_LEFT, new int[]{0x7f000000, 0x00000000});
			rightShadow = new GradientDrawable(Orientation.LEFT_RIGHT, new int[]{0x7f000000, 0x00000000});
			bottomShadow = new GradientDrawable(Orientation.TOP_BOTTOM, new int[]{0x7f000000, 0x00000000});
		}
		public void disableShadow(){
			this.bShadow = false;
		}

		protected void draw(Canvas canvas){
			if(backgroundImage != null)
				canvas.drawBitmap(backgroundImage, null, rect, null);
			if(topImage != null){
				int height = topRect.height();
				topRect.top = rect.top;
				topRect.bottom = rect.top+height;
				topRect.left = rect.left;
				topRect.right = rect.right;
				topSrcRect.top = topRect.top;
				topSrcRect.bottom = topRect.bottom;
				topSrcRect.left = topImage.getWidth()/2 - topRect.width()/2;
				topSrcRect.right = topImage.getWidth()/2 + topRect.width()/2;
				canvas.drawBitmap(topImage, topSrcRect, topRect, null);
				if(bShadow){
					bottomShadow.setBounds(topRect.left, topRect.bottom, topRect.right, topRect.bottom+shadowWidth);
					bottomShadow.draw(canvas);
					leftShadow.setBounds(topRect.left - shadowWidth, topRect.top, topRect.left, topRect.bottom);
					leftShadow.draw(canvas);
					rightShadow.setBounds(topRect.right, topRect.top, topRect.right + shadowWidth, topRect.bottom);
					rightShadow.draw(canvas);
				}
			}
			if(bottomImage != null){
				int height = topRect.height();
				bottomRect.top = rect.bottom-height;
				bottomRect.bottom = rect.bottom;
				bottomRect.left = rect.left;
				bottomRect.right = rect.right;
				canvas.drawBitmap(bottomImage, null, bottomRect, null);
				if(bShadow){
					bottomShadow.setBounds(bottomRect.left, bottomRect.bottom, bottomRect.right, bottomRect.bottom+shadowWidth);
					bottomShadow.draw(canvas);
					leftShadow.setBounds(bottomRect.left - shadowWidth, bottomRect.top, bottomRect.left, bottomRect.bottom);
					leftShadow.draw(canvas);
					rightShadow.setBounds(bottomRect.right, bottomRect.top, bottomRect.right + shadowWidth, bottomRect.bottom);
					rightShadow.draw(canvas);
				}
			}
			if(leftImage != null){
				int width = leftRect.width();
				leftRect.top = rect.top+topRect.height();
				leftRect.bottom = rect.bottom-bottomRect.height();
				leftRect.left = rect.left;
				leftRect.right = rect.left + width;
				canvas.drawBitmap(leftImage, null, leftRect, null);
				leftShadow.setBounds(leftRect.left - shadowWidth, leftRect.top, leftRect.left, leftRect.bottom);
				leftShadow.draw(canvas);
				if(bShadow){
					rightShadow.setBounds(leftRect.right, leftRect.top, leftRect.right+shadowWidth, leftRect.bottom);
					rightShadow.draw(canvas);
				}
			}
			if(rightImage != null){
				int width = rightRect.width();
				rightRect.top = rect.top+topRect.height();
				rightRect.bottom = rect.bottom-bottomRect.height();
				rightRect.left = rect.right - width;
				rightRect.right = rect.right;
				canvas.drawBitmap(rightImage, null, rightRect, null);
				leftShadow.setBounds(rightRect.left - shadowWidth, rightRect.top, rightRect.left, rightRect.bottom);
				leftShadow.draw(canvas);
				//left to right
				if(bShadow){
					rightShadow.setBounds(rightRect.right, rightRect.top, rightRect.right+shadowWidth, rightRect.bottom);
					rightShadow.draw(canvas);
				}
			}

		}
		/**
		 * left, right set을 나눠 놓지 않은 이유는, 크기제한을 체크하기 위하여
		 * @param left
		 * @param right
		 * @return size가 최대, 최소크기 사이를 벗어나면 false, transformable이 아닌데, 사이즈가 바뀐다면 false
		 */
		protected void setLeftRight(int left, int right){
			int width = right - left;
			if(!transformable && width != rect.width())
				return;

			float dLeft = Math.abs((float)left - rect.left);
			float dRight = Math.abs((float)right - rect.right);
			
			if(right - left < minSize){
				if(dLeft > dRight)
					left = right-minSize;
				else
					right = left+minSize;
			}
			if(right - left > maxSize){//사용자 설정에 따른 크기제한{
				if(dLeft > dRight)
					left = right-maxSize;
				else
					right = left+maxSize;
			}
			if(left < ThumbnailSeekBar.this.getLeft())
				left = ThumbnailSeekBar.this.getLeft();
			if(right > Math.min(ThumbnailSeekBar.this.getRight(), listWidth))//뷰의 크기에 따른 크기제한
				right = (int) Math.min(ThumbnailSeekBar.this.getRight(), listWidth);
//			if(getLastVisiblePosition() == getAdapter().getCount()-1 && getChildAt(getChildCount()-1).getRight() <= getRight())//뷰의 크기보다 썸네일이 적을경우 마지막 썸네일까지만
//				if(right > getChildAt(getChildCount()-1).getRight())
//					result = false;

			rect.right = right;
			rect.left = left;
			if(beltListener != null && dLeft != dRight){
				if(dLeft > dRight){
					uiHandler.post(new Runnable() {
						@Override
						public void run() {
							beltListener.onBeltLeftResizing(xToTime(getScrolledX()+rect.left));
						}
					});
				}else{
					uiHandler.post(new Runnable() {
						@Override
						public void run() {
							beltListener.onBeltRightResizing(xToTime(getScrolledX()+rect.right));
						}
					});
				}
			}
		}
		protected int getLeft(){
			return rect.left;
		}
		protected int getRight(){
			return rect.right;
		}
		protected int width(){
			return rect.width();
		}
		protected int centerX(){
			return rect.centerX();
		}
		protected void setCenterX(int x){
			//			for(int i=0; i<Thread.currentThread().getStackTrace().length; i++)
			//			Log.d("belt", "setCenter");
			int width = rect.width();
			rect.left = x - width/2;
			rect.right = x + width/2;
			if(beltListener != null){
				uiHandler.post(new Runnable() {

					@Override
					public void run() {
						// TODO Auto-generated method stub
						beltListener.onBeltMoved(xToTime(getScrolledX()+rect.left), xToTime(getScrolledX()+rect.right));
					}
				});
			}
		}
		public void enableTransform(int minSize, int maxSize, int transformableEdgeSize){
			this.transformable = true;
			this.transformableEdgeSize = transformableEdgeSize;
			this.minSize = minSize;
			this.maxSize = maxSize;
		}
		public void disableTransform(){
			transformable = false;
		}
		public Rect getTransformableLeftRect(){
			return new Rect(rect.left, rect.top, rect.left+transformableEdgeSize, rect.bottom);
		}
		public Rect getTransformableRightRect(){
			return new Rect(rect.right-transformableEdgeSize, rect.top, rect.right, rect.bottom);
		}
		protected boolean containInLeftEdge(int x){
			return getTransformableLeftRect().left <= x && getTransformableLeftRect().right >= x;
		}
		protected boolean containInRightEdge(int x){
			return getTransformableRightRect().left <= x && getTransformableRightRect().right >= x;
		}
		public boolean isTransformable(){
			return transformable;
		}
	}

	/**
	 * scroll로 인한 seeking시, scroll drag seeking중일경우 onSeeking이 매번 호출되고
	 * drag seeking이 끝나거나, fling이 끝날경우 onSeeked가 호출됌.
	 * fling시에는 onSeeking이 호출되지 않는다. 
	 * @author bigs
	 *
	 */
	public static interface OnSeekListener{
		void onSeeking(long seekingTime);
		void onSeeked(long seekedTime);
	}
	public static interface BeltListener{
		void onBeltLeftResizing(int time);
		void onBeltRightResizing(int time);
		void onBeltResized(int leftTime, int rightTime);
		void onBeltMoved(int leftTime, int rightTime);
	}
}
