package com.example.zzalit_bigs;

import it.sephiroth.android.library.widget.AbsHListView;
import it.sephiroth.android.library.widget.HListView;

import java.io.File;
import java.io.IOException;
import java.lang.reflect.Field;
import java.lang.reflect.Method;
import java.util.LinkedHashMap;
import java.util.LinkedHashSet;

import android.annotation.SuppressLint;
import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.os.Handler;
import android.os.Looper;
import android.util.AttributeSet;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.ImageView.ScaleType;
import android.widget.RelativeLayout;

import com.nostra13.universalimageloader.core.DisplayImageOptions;
import com.nostra13.universalimageloader.core.ImageLoader;
import com.nostra13.universalimageloader.core.assist.FailReason;
import com.nostra13.universalimageloader.core.assist.ImageLoadingListener;
import com.nostra13.universalimageloader.utils.L;

/**
 * 
 * 
 * ---------------------deprecated-----------------------------
 * 영상을 넣으면 tumbnail리스트를 뽑아내줌
 * 한 화면에 보이는 썸네일의 갯수만큼의 썸네일 꾸러미를 구성하고(ThumbBundle)
 * 필요할 경우 꾸러미 단위로 동영상에서 추출한다.
 * 꾸러미의 크기는 displayWidth/ThumbnailWidth 이다.
 * 실제 썸네일 이미지는 설정한 저장소/ThumbBundleId/thumbnailId.jpeg로 저장되며, id들은 모두 정수이며, 동영상 시간과 관계된다.
 * 
 * --bundle, thumb의 동영상 시간과의 관계
 * 먼저 bndle의 크기를 n이라 하고 동영상 전체 시간을 T, 한 화면에 보이는 시간을 t라 하자.
 *이때, 한 썸네일당 가리키는 영상의 시간크기는 t/n이 될것이다. 이를 t'이라 하자.
 *
 *현재 플레이중인 시간을 ct 라 할때,
 *위와 같은 관계에서 
 *ThumbBundleId = ct/t, 
 *ThumbId = t/t'
 *이 나오며
 *
 *역산하면, t = ThumbId*t', ct = t*ThumbBundleId  ->  ct = ThumbId*t'* ThumbBundleId*t = ThumbId*(t/n)*ThumbBundleId*t 이다.
 *이렇게하면 bundleId와 thumbId를 통해 그 썸네일이 가리키는 시간을 알수 있다.
 *
 *예를 들어 (ThumBundleId, ThumbId) = (10,3)이고, n이 6, t가 60이라면,  
 *그 썸네일이 가르키는 시간은 630~640초가 된다.
 * 
 *
 *-----------------------------new!--------------------
 *사진을 여러장 뽑을경우 한번에뽑는게 빠르긴하지만, 여러장을 뽑을때까지 디스플레이하기 힘들다.(외부프로그램의 결과. 이에대한 관리가 힘듬)
 *따라서 실제 n장을 뽑는데에는 느리더라도, 한장 뽑을때마다 보여줄수 있도록하여
 *사용자가 보기에 반응성을 더 높혔다. 또한 관리도 편 ㅋ 리 ㅋ 
 *걍  thumbnail의 크기를 정하고,  listitem의 position이 곧 썸네일 아이디가 되며,
 *thumbnailId * thumbnailtime이 그 썸네일이 표시하는 시간. 
 *파일저장도 그냥 position.jpeg로 저장!
 *
 *차후에 이전버전처럼 꾸러미로 관리하면서도, 반응성을 높히는 방법을 구상해보자.
 *
 * 
 *TODO : 현재, 이론상으로 쓰레드가 무한개로 생길수 있다. 이는 분명히 문제가 되나, 실제적으로 인간이 컨트롤할때 생길수 있는 쓰레드 수는 cntThumbsPerDisplay*3을 넘기힘들다.
 *단, cpu가 문제가 있어 처리가 지연될경우 무한히 생길수 있다.
 *이를 해결하기 위해 쓰레드 풀을 사용할 수 있겠지만 이전쓰레드가 실행하고 있는 프로세스를 종료하지 않는 이상 반응성이 너무 느려지게 된다.
 *따라서  견고함을 위해 처리할 필요가 있다.
 * @author bigs
 *
 */
public class VideoThumbGeneratingListView extends HListView
{
	private boolean onTouch = false;
	protected boolean isSeeking = false;
	protected boolean isScrolling = false;
	//	private LinkedHashSet<Integer> extractableSet = null;//추출허가. getView에서 항상 썸네일을 뽑아내는건 손해, 특정경우에만 썸네일을 뽑아내는것을 허가한다.(스크롤링할때 모든 썸네일을 뽑아내게 할순없지.)
	private LinkedHashSet<Integer> displayedPositionSet = null;
//	private LinkedHashMap<Integer, Long> imageViewAndControlThread = null;//imageview의 id와 그에대한 조작권한을 가진 쓰레드 아이디
	private LinkedHashSet<Integer> bundleSet = null;//추출허가된 꾸러미들
	protected long videoTotalTime = 0;
	protected long listWidth = 0;
	protected float timePerPixel = 0;
	//	protected long currentTime = 0;

	protected String videoPath = null;
	protected String tmpDir = null;
	protected int thumbWidth = 0;
	protected int cntThumbsPerScreen = 0; //한 화면에 보이는 썸네일 수 
	protected long thumbTime = 0;//한 thumb이 나타내는 시간
	private VideoEditor editor = null;
	private Handler uiHandler;

	public VideoThumbGeneratingListView(Context context, AttributeSet attrs,
			int defStyle) {
		super(context, attrs, defStyle);
		init();
		// TODO Auto-generated constructor stub
	}

	public VideoThumbGeneratingListView(Context context, AttributeSet attrs) {
		super(context, attrs);
		init();
		// TODO Auto-generated constructor stub
	}

	public VideoThumbGeneratingListView(Context context) {
		super(context);
		init();
		// TODO Auto-generated constructor stub
	}
	private void init(){
		//		extractableSet = new LinkedHashSet<Integer>();
		bundleSet = new LinkedHashSet<Integer>();
		displayedPositionSet = new LinkedHashSet<Integer>();
//		imageViewAndControlThread = new LinkedHashMap<Integer, Long>();
		uiHandler = new Handler(Looper.getMainLooper());

		//		setAdapter(new ThumbAdapter());
		setOnScrollListener(new OnScrollListener() {

			@Override
			public void onScrollStateChanged(AbsHListView view, int scrollState) {
				// TODO Auto-generated method stub
				if(scrollState == 0 && !onTouch ){//조작이 멈췄을경우 
					int pos = getFirstVisiblePosition();
					int bundleId = pos/cntThumbsPerScreen;
					for(int i=0; i<3; i++){
						if(!bundleSet.contains(bundleId+i)){
							bundleSet.add(bundleId+i);
							new ThumbExtracter(bundleId+i).start();
							Log.d("thumb", "extracting bundle : "+(bundleId+i));
						}
					}
					((BaseAdapter)getAdapter()).notifyDataSetChanged();
					isScrolling = false;
					if(isSeeking){
						onSeekingStateChanged(false);
					}
				}else{
					isScrolling = true;
				}
			}
			
			@Override
			public void onScroll(AbsHListView view, int firstVisibleItem,
					int visibleItemCount, int totalItemCount) {
				// TODO Auto-generated method stub
			}
		});
	}
	protected void onSeekingStateChanged(boolean isScrollable){
	}
	/**
	 * listview의 사이즈가 정해진 후에 사용해야만 정상적인 결과를 얻을 수 있다.
	 * @param tmpDir 썸네일들이 추출되어 일시적으로 저장될 임시폴더
	 * @param videoPath
	 * @param videoTotalTime
	 * @param thumbWidthPx
	 * @param timePerBundle
	 */
	public void startExtractThumbs(String tmpDir, String videoPath, long videoTotalTime, int thumbWidthPx, long timePerThumb){

		this.tmpDir = tmpDir;
		this.videoPath = videoPath;
		this.videoTotalTime = videoTotalTime;
		this.thumbWidth = thumbWidthPx;
		this.thumbTime = timePerThumb;
		this.timePerPixel = timePerThumb/(float)thumbWidthPx;
		this.cntThumbsPerScreen = (int) Math.ceil((getWidth()/(double)thumbWidthPx));
		this.listWidth = thumbWidth*((int)(videoTotalTime/thumbTime));
		File tmpDirectory = new File(this.tmpDir);//이전 정보 제거
		if(!tmpDirectory.exists())
			tmpDirectory.mkdir();
		File[] preThumbs = tmpDirectory.listFiles();
		for(File thumb : preThumbs)
			thumb.delete();

		editor = new VideoEditorBuilder(getContext())
		.images(true)
		.setFps(1.0f/(thumbTime/1000))
		.setSize(64, -2)
		.setWorkingDirectory(tmpDir)
		.build();

		setAdapter(new ThumbAdapter());
		bundleSet.add(0);
		new ThumbExtracter(0).start();
		bundleSet.add(1);
		new ThumbExtracter(1).start();
		Log.d("bigs_info","totalTiem : "+videoTotalTime+", thumbTime :"+thumbTime+", thumbWidth : "+thumbWidth+" listWidth:"+listWidth);
	}
	public boolean isSeeking(){
		Log.d("bigs", "isSeeking : "+isSeeking);
		return isSeeking;
	}
	@Override
	@SuppressLint("Override")
	public boolean onTouchEvent(MotionEvent ev) {
		// TODO Auto-generated method stub
		switch(ev.getAction()){
		case MotionEvent.ACTION_DOWN:
			onTouch = true;
			if(!isSeeking){
				onSeekingStateChanged(true);
			}
			break;
		case MotionEvent.ACTION_MOVE:
			break;
		case MotionEvent.ACTION_UP:
			onTouch = false;
			if(!isScrolling){
				onSeekingStateChanged(false);
			}
//			smoothScrollBy(0, 0);
			break;
		}
		return super.onTouchEvent(ev);
	}
	//	private void stopScroll(AbsHListView view)
	//	{
	//	    try
	//	    {
	//	        Field field = android.widget.AbsListView.class.getDeclaredField("mFlingRunnable");
	//	        field.setAccessible(true);
	//	        Object flingRunnable = field.get(view);
	//	        if (flingRunnable != null)
	//	        {
	//	            Method method = Class.forName("it.sephiroth.android.library.widget.AbsHListView$FlingRunnable").getDeclaredMethod("endFling");
	//	            method.setAccessible(true);
	//	            method.invoke(flingRunnable);
	//	        }
	//	    }
	//	    catch (Exception e) {}
	//	}
	protected int timeToX(long time){
		//		Log.d("bigs", "timeToX time:"+time+", x:"+(time * 1/timePerPixel)+", timePerPixel:"+timePerPixel);
		return (int) (time * 1/timePerPixel);
	}
	protected int xToTime(int x){
		return (int) (x*timePerPixel);
	}

	private class ThumbAdapter extends BaseAdapter{

		private DisplayImageOptions thumbDisplayOptions = null;
		private LayoutInflater inflater = null;
		public ThumbAdapter() {
			// TODO Auto-generated constructor stub
			setDisplayImageOptions();
			inflater = LayoutInflater.from(getContext());
		}

		@Override
		public int getCount() {
			// TODO Auto-generated method stub
			return (int) (videoTotalTime/thumbTime);
		}

		@Override
		public Object getItem(int position) {
			// TODO Auto-generated method stub
			return position;
		}

		@Override
		public long getItemId(int position) {
			// TODO Auto-generated method stub
			return position;
		}
		@Override
		public boolean isEnabled(int position) {
			// TODO Auto-generated method stub
			return false;
		}
		@Override
		public View getView(final int position, View convertView, ViewGroup parent) {
			// TODO Auto-generated method stub
			if(convertView == null){
				convertView = inflater.inflate(R.layout.film_item, null);
				convertView.setId(position);
			}
			ImageView image = (ImageView)convertView.findViewById(R.id.iv_film_content);
			convertView.setLayoutParams(new HListView.LayoutParams(thumbWidth, getHeight()));
			ImageLoader.getInstance().displayImage("file://"+tmpDir+"/"+position+".jpeg",
					image, thumbDisplayOptions, new ImageLoadingListener() {

				@Override
				public void onLoadingStarted(String imageUri, View view) {

				}
				@Override
				public void onLoadingFailed(String imageUri, View view,
						FailReason failReason) {
					Log.d("bigs","load fail");
				}
				@Override
				public void onLoadingComplete(String imageUri, View view, Bitmap loadedImage) {
//					Log.d("bigs","image load complete");
					((ImageView)view).setImageBitmap(loadedImage);
					displayedPositionSet.add(position);
					view.invalidate();
				}
				@Override
				public void onLoadingCancelled(String imageUri, View view) {

				}
			});
			//				}
			//			}
			//			if(extractableSet.contains(position)){
			//				Thread th = new ThumbExtracter(position, (ImageView) convertView);
			//				imageViewAndControlThread.put(convertView.getId(), th.getId());
			//				th.start();
			//			}
			return convertView;
		}

		protected void setDisplayImageOptions() {
			ImageLoader.getInstance().clearMemoryCache();
			if (thumbDisplayOptions == null) {
				thumbDisplayOptions = new DisplayImageOptions.Builder()
				.showImageOnLoading(new ColorDrawable(getResources().getColor(R.color.gray_3)))
				.showImageOnFail(new ColorDrawable(getResources().getColor(R.color.gray_3)))
				.cacheInMemory(true)
				.bitmapConfig(Bitmap.Config.RGB_565) // 용량을 1/2정도로 아낄 수 있지만, blur 효과가 이상하게 들어감.
				.build();
				L.disableLogging();
			}
		}
	}
	private class ThumbExtracter extends Thread{

		//			private ImageView view;
		private int bundleId;
		public ThumbExtracter(int bundleId) {
			// TODO Auto-generated constructor stub
			//				this.view = view;
			//				this.position = position;
			this.bundleId = bundleId;
		}

		public void run() {
			for(int i=0; i< cntThumbsPerScreen; i++){
				final int pos = (bundleId*cntThumbsPerScreen+i);
				File thumb = new File(tmpDir+"/"+pos+".jpeg");
				//				Log.d("bigs","image load : ("+position+") "+thumb.exists());
				if(!thumb.exists()){
					try {
//						Log.d("thumb","make thumbnail : "+pos);
						editor.makeThumbnail(pos*thumbTime, videoPath , pos+".jpeg");
					} catch (IOException e) {
						// TODO Auto-generated catch block
						e.printStackTrace();
					}
				}
				uiHandler.post(new Runnable(){
					@Override
					public void run() {
						// TODO Auto-generated method stub
//						Log.d("thumb","extracted thumb : "+pos);
						if(getFirstVisiblePosition() <= pos && pos <= getLastVisiblePosition())
							((BaseAdapter)getAdapter()).notifyDataSetChanged();
					}

				});
			};
		}
	}
}
