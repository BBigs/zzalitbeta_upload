package com.example.zzalit_bigs;

import java.io.File;
import java.io.IOException;

import android.graphics.BitmapFactory;
import android.graphics.drawable.ColorDrawable;
import android.media.MediaPlayer;
import android.os.Bundle;
import android.util.TypedValue;
import android.view.ViewGroup.LayoutParams;
import android.widget.ScrollView;

import com.actionbarsherlock.app.ActionBar;
import com.actionbarsherlock.app.SherlockActivity;
import com.actionbarsherlock.view.Menu;
import com.actionbarsherlock.view.MenuItem;
import com.example.zzalit_bigs.LoopingVideoView.OnPreparedListener;

public class UploadPostActivity extends SherlockActivity{
	public static final String TAG = "UploadPostActivity";
	public static final int ITEM_POST_COMPLETE = 2;
	

	private String videoUrl = null;
	private File jpegsDir = null;
	private boolean isSoundOn = false;
	private int playMode = 0;
	private float playSpeed = 1.0f;
	
	private ScrollView rootView = null;
	private LoopingVideoView loopingVideo = null;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_upload_post);
		videoUrl = getIntent().getStringExtra("url");
		jpegsDir = new File(getIntent().getStringExtra("jpegsDir"));
		isSoundOn = getIntent().getBooleanExtra("isSoundOn", false);
		playMode = getIntent().getIntExtra("playMode", LoopingVideoView.PLAY_MODE_NORMAL);
		playSpeed = getIntent().getFloatExtra("playSpeed", 1.0f);
		ActionBar actionBar = getSupportActionBar();
		actionBar.setDisplayHomeAsUpEnabled(true);
		actionBar.setDisplayShowHomeEnabled(false);
		actionBar.setTitle("업로드");
		actionBar.setBackgroundDrawable(new ColorDrawable(getResources().getColor(R.color.upload_actionbar_color)));
		rootView = (ScrollView) findViewById(R.id.layout_upload_post);
		loopingVideo = (LoopingVideoView)rootView.findViewById(R.id.video_result_view);
		try {
			loopingVideo.setDataSource(videoUrl, jpegsDir.getAbsolutePath());
		} catch (IllegalArgumentException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (SecurityException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (IllegalStateException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		loopingVideo.prepareAsync();
		loopingVideo.setOnPreparedListener(new OnPreparedListener() {
			
			@Override
			public void onPrepared(MediaPlayer mp) {
				// TODO Auto-generated method stub
				mp.setVolume(isSoundOn?1.0f:0.0f, isSoundOn?1.0f:0.0f);
				loopingVideo.start(playMode);
				loopingVideo.setPlaySpeed(playSpeed);
			}
		});
		
	}
	@Override
	public void onDestroy() {
		// TODO Auto-generated method stub
		loopingVideo.recycle();
		super.onDestroy();
	}
	
	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		// TODO Auto-generated method stub
		menu.clear();
		MenuItem itemComplete = menu.add(Menu.NONE, ITEM_POST_COMPLETE, Menu.NONE, "post_complete");
		itemComplete.setIcon(R.drawable.upload_video_complete_button);
		itemComplete.setShowAsAction(MenuItem.SHOW_AS_ACTION_ALWAYS);
		return true;
	}
	
	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		// TODO Auto-generated method stub
		switch (item.getItemId()) {
		case ITEM_POST_COMPLETE:
			break;
		}
		return true;
	}
}
