package com.example.zzalit_bigs;

import java.io.File;
import java.io.IOException;

import android.content.Context;
import android.graphics.Color;
import android.media.MediaPlayer;
import android.media.MediaPlayer.OnPreparedListener;
import android.os.Bundle;
import android.os.Environment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.View.OnTouchListener;
import android.view.ViewGroup;
import android.view.ViewGroup.LayoutParams;
import android.view.animation.AlphaAnimation;
import android.view.animation.Animation;
import android.view.animation.Animation.AnimationListener;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.Toast;
import android.widget.VideoView;

import com.actionbarsherlock.app.SherlockFragment;
import com.actionbarsherlock.view.Menu;
import com.actionbarsherlock.view.MenuInflater;
import com.actionbarsherlock.view.MenuItem;

public class UploadVideoCutFragment extends SherlockFragment{

	public static final String TAG = "UploadVideoCutFragment";
	private static final int ITEM_CUT = 0;

	static UploadVideoCutFragment newInstance(String videoUrl){
		UploadVideoCutFragment fragment = new UploadVideoCutFragment();
		Bundle bundle = new Bundle();
		bundle.putString("url", videoUrl);
		fragment.setArguments(bundle);
		return fragment;
	}

	private ImageView ivPlay = null;
	private ImageView ivPause = null;
	private VideoView videoView = null;
	private RelativeLayout videoViewWrapper = null;
	private AlphaAnimation animPlayImage = null;
	private AlphaAnimation animPauseImage = null;
	private MediaPlayer mp = null;
	
	private String videoUrl = null;
	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container,
			Bundle savedInstanceState) {
		// TODO Auto-generated method stub
        setHasOptionsMenu(true);
		videoUrl = getArguments().getString("url");
		ViewGroup rootView = (ViewGroup)inflater.inflate(R.layout.fragment_upload_video_cut, null);
		videoView = (VideoView)rootView.findViewById(R.id.video_cut_display);
		videoViewWrapper = (RelativeLayout)rootView.findViewById(R.id.video_cut_display_wrapper);
		ivPlay = (ImageView)rootView.findViewById(R.id.iv_video_cut_play);
		ivPause = (ImageView)rootView.findViewById(R.id.iv_video_cut_pause);
		
//		videoView.setZOrderOnTop(true);
		
		animPlayImage = new AlphaAnimation(0.8f, 0.0f);
		animPlayImage.setDuration(800);
		animPauseImage = new AlphaAnimation(0.8f, 0.0f);
		animPauseImage.setDuration(800);

		rootView.setOnTouchListener(new OnTouchListener() {

			@Override
			public boolean onTouch(View v, MotionEvent event) {
				// TODO Auto-generated method stub
				return true;//뒤에 있는 프래그먼트가 터치를 먹지 않게.
			}
		});
		videoView.setOnPreparedListener(new OnPreparedListener() {

			@Override
			public void onPrepared(MediaPlayer mp) {
				// TODO Auto-generated method stub
				UploadVideoCutFragment.this.mp = mp;
				float scale = videoViewWrapper.getWidth()/(float)mp.getVideoWidth();
				int scaledHeight = (int) (mp.getVideoHeight()*scale);
				if(scaledHeight <= videoViewWrapper.getHeight()){
					LayoutParams params = videoViewWrapper.getLayoutParams();
					params.height = scaledHeight; //사실 params가 reference라서 set안해도 적용되긴함. for 안전
					videoViewWrapper.setLayoutParams(params);
				}else{
					LayoutParams params = videoViewWrapper.getLayoutParams();
					params.height = videoViewWrapper.getHeight(); //사실 params가 reference라서 set안해도 적용되긴함. for 안전
					videoViewWrapper.setLayoutParams(params);
				}
				videoView.setBackgroundColor(Color.TRANSPARENT);
				playVideo();
			}
		});
		videoView.setOnTouchListener(new OnTouchListener() {
			
			@Override
			public boolean onTouch(View v, MotionEvent event) {
				// TODO Auto-generated method stub
				if(event.getAction() == MotionEvent.ACTION_DOWN){
					if(videoView.isPlaying())
						pauseVideo();
					else
						playVideo();
				}
				return true;
			}
		});
		videoView.setVideoPath(videoUrl);
		return rootView;
	}

	private void playVideo(){
		videoView.start();
		ivPlay.setVisibility(View.VISIBLE);
		animPlayImage.setAnimationListener(new AnimationListener() {

			@Override
			public void onAnimationStart(Animation animation) {
				// TODO Auto-generated method stub

			}

			@Override
			public void onAnimationRepeat(Animation animation) {
				// TODO Auto-generated method stub

			}

			@Override
			public void onAnimationEnd(Animation animation) {
				// TODO Auto-generated method stub
				ivPlay.setVisibility(View.GONE);
			}
		});
		ivPlay.startAnimation(animPlayImage);
	}
	private void pauseVideo(){
		videoView.pause();
		ivPause.setVisibility(View.VISIBLE);
		animPauseImage.setAnimationListener(new AnimationListener() {

			@Override
			public void onAnimationStart(Animation animation) {
				// TODO Auto-generated method stub

			}

			@Override
			public void onAnimationRepeat(Animation animation) {
				// TODO Auto-generated method stub

			}

			@Override
			public void onAnimationEnd(Animation animation) {
				// TODO Auto-generated method stub
				ivPause.setVisibility(View.GONE);
			}
		});
		ivPause.startAnimation(animPauseImage);
	}
	public void restore(){
		if(videoView.getVisibility() != View.VISIBLE){
			videoView.setVisibility(View.VISIBLE);
		}
	}
	public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
		// TODO Auto-generated method stub
		menu.clear();
		MenuItem itemCut = menu.add(Menu.NONE, ITEM_CUT, Menu.NONE, "cut");
		itemCut.setIcon(R.drawable.upload_cut_video_button);
		itemCut.setShowAsAction(MenuItem.SHOW_AS_ACTION_ALWAYS);
	}
	
	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		// TODO Auto-generated method stub
		switch(item.getItemId()){
		case ITEM_CUT :
			if(videoView.isPlaying())
				pauseVideo();
			File editVideo = new File(Environment.getExternalStorageDirectory(), "editing.mp4");// new File(getActivity().getDir("edit", Context.MODE_PRIVATE), "editing_video.mp4");
			File jpegsDir = new File(Environment.getExternalStorageDirectory(), "test");//getActivity().getDir("jpegs", Context.MODE_PRIVATE);
			if(!jpegsDir.exists())
				jpegsDir.mkdir();
			Log.d("bigs"," " + editVideo.getAbsolutePath()+" cut!");
			Toast.makeText(getActivity(), editVideo.getAbsolutePath()+" cut!", Toast.LENGTH_SHORT).show();
			try {
				new VideoEditorBuilder(getActivity())
					.setExtractTime(0, 10)
					.setFps(25)
					.build().edit(videoUrl, editVideo.getAbsolutePath());
				new VideoEditorBuilder(getActivity())
					.setFps(25)
					.images(true)
					.build().edit(editVideo.getAbsolutePath(), jpegsDir.getAbsolutePath()+"/%d.jpeg");
			} catch (IOException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			} catch (InterruptedException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			FragmentManager fm = getSherlockActivity().getSupportFragmentManager();
			fm.beginTransaction().setTransition(FragmentTransaction.TRANSIT_FRAGMENT_OPEN)
			.add(R.id.upload_fragment_frame, UploadVideoConfigFragment.newInstance(editVideo.getAbsolutePath(), jpegsDir.getAbsolutePath()), UploadVideoConfigFragment.TAG)
			.addToBackStack(null).commit();
//			videoView.setVisibility(View.INVISIBLE);//surface가 겹치는걸 막기위해
//			fm.beginTransaction().remove(this).commit();
		}
		return false;
	}
	
	@Override
	public void onDestroy() {
		// TODO Auto-generated method stub
		if(videoView.isPlaying())
			videoView.pause();
		super.onDestroy();
	}
}
