package com.example.zzalit_bigs;

import java.io.File;
import java.io.IOException;

import android.content.Context;
import android.content.Intent;
import android.graphics.BitmapFactory;
import android.graphics.drawable.ColorDrawable;
import android.media.MediaPlayer;
import android.os.Bundle;
import android.os.Handler;
import android.os.Looper;
import android.util.Log;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.View.OnTouchListener;
import android.widget.Button;
import android.widget.ImageButton;
import android.widget.RelativeLayout;
import android.widget.Toast;

import com.actionbarsherlock.app.ActionBar;
import com.actionbarsherlock.app.SherlockActivity;
import com.actionbarsherlock.view.Menu;
import com.actionbarsherlock.view.MenuItem;
import com.example.zzalit_bigs.LoopingVideoView.OnPreparedListener;
import com.example.zzalit_bigs.ThumbnailSeekBar.Belt;
import com.example.zzalit_bigs.ThumbnailSeekBar.BeltListener;
import com.example.zzalit_bigs.ThumbnailSeekBar.OnSeekListener;

public class UploadVideoConfigActivity extends SherlockActivity{

	public static final String TAG = "UploadVideoConfigActivity";
	//actionbar menu item
	private static final int ITEM_CONFIG_COMPLETE = 1;

	private String videoUrl = null;
	private File jpegsDir = null;
	private RelativeLayout rootView = null;
	private Button btPlaySpeed = null;
	private ImageButton btPlayMode = null;
	private ImageButton btSoundConfig = null;
	private LoopingVideoView loopingVideo = null;
	private ThumbnailSeekBar sbThumbs = null;
	private File thumbsDir = null;
	private Handler uiHandler = null;

	private boolean isSoundOn = true;
	private boolean soundEnable = true;
	private MediaPlayer mp = null;
	private long duration = 0;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_upload_video_config);
		LayoutInflater inflater = (LayoutInflater)getSystemService(Context.LAYOUT_INFLATER_SERVICE);
		videoUrl = getIntent().getStringExtra("url");
		duration = getIntent().getLongExtra("duration", 0);
		jpegsDir = new File(getIntent().getStringExtra("jpegsDir"));
		uiHandler = new Handler(Looper.getMainLooper());

		View actionBarLayout = inflater.inflate(R.layout.actionbar_video_config, null);
		
		ActionBar actionBar = getSupportActionBar();
		actionBar.setBackgroundDrawable(new ColorDrawable(getResources().getColor(R.color.theme_blue)));
		actionBar.setTitle("업로드");
		ActionBar.LayoutParams lp = new ActionBar.LayoutParams(ActionBar.LayoutParams.WRAP_CONTENT, ActionBar.LayoutParams.WRAP_CONTENT,
        		Gravity.CENTER);
		actionBar.setCustomView(actionBarLayout, lp);
        getSupportActionBar().setDisplayShowCustomEnabled(true);
        getSupportActionBar().setDisplayShowHomeEnabled(false);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
		rootView = (RelativeLayout) findViewById(R.id.layout_upload_video_config);
		loopingVideo = (LoopingVideoView)rootView.findViewById(R.id.video_config_display);
		sbThumbs = (ThumbnailSeekBar)rootView.findViewById(R.id.sb_video_config_film_bar);
		btPlaySpeed = (Button)rootView.findViewById(R.id.tv_play_speed);
		btPlayMode = (ImageButton)rootView.findViewById(R.id.bt_play_mode);
		btSoundConfig = (ImageButton)rootView.findViewById(R.id.bt_upload_sound_config);

		btPlaySpeed.setText(String.format("X%.1f", loopingVideo.getPlaySpeed()));

		rootView.setOnTouchListener(new OnTouchListener() {

			@Override
			public boolean onTouch(View v, MotionEvent event) {
				// TODO Auto-generated method stub
				return true;//뒤에 있는 프래그먼트가 터치를 먹지 않게.
			}
		});
		loopingVideo.setOnPreparedListener(new OnPreparedListener() {

			@Override
			public void onPrepared(MediaPlayer mp) {
				// TODO Auto-generated method stub
				UploadVideoConfigActivity.this.mp = mp;
				loopingVideo.start();

				uiHandler.post(new Runnable() {
					@Override
					public void run() {
						sbThumbs.setSeekBarPos(sbThumbs.timeToX(loopingVideo.getCurrentTime()));
						uiHandler.postDelayed(this, 30);
					}
				});
			}
		});
		btPlaySpeed.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				loopingVideo.setPlaySpeed(loopingVideo.getPlaySpeed()+0.5f);
				if(loopingVideo.getPlaySpeed() > LoopingVideoView.PLAY_SPEED_MAX)
					loopingVideo.setPlaySpeed(LoopingVideoView.PLAY_SPEED_MIN);
				if(loopingVideo.getPlaySpeed() == 1.0f){
					soundOff();
					soundEnable = true;
				}
				else
					soundDisable();
				btPlaySpeed.setText(String.format("X%.1f", loopingVideo.getPlaySpeed()));
			}
		});

		btPlayMode.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				switch(loopingVideo.getMode()){
				case LoopingVideoView.PLAY_MODE_NORMAL:
					btPlayMode.setImageResource(R.drawable.upload_play_mode_button_reverse);
					loopingVideo.setMode(LoopingVideoView.PLAY_MODE_REVERSE);
					soundDisable();
					break;
				case LoopingVideoView.PLAY_MODE_REVERSE:
					btPlayMode.setImageResource(R.drawable.upload_play_mode_button_pingpong);
					loopingVideo.setMode(LoopingVideoView.PLAY_MODE_PINGPONG);
					soundDisable();
					break;
				case LoopingVideoView.PLAY_MODE_PINGPONG:
					btPlayMode.setImageResource(R.drawable.upload_play_mode_button_normal);
					loopingVideo.setMode(LoopingVideoView.PLAY_MODE_NORMAL);
					soundOff();
					soundEnable = true;
					break;
				}
			}
		});

		btSoundConfig.setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				if(!soundEnable)
					return;
				if(isSoundOn){
					soundOff();
				}else{
					soundOn();
				}
			}
		});
		try {
			loopingVideo.setDataSource(videoUrl, jpegsDir.getAbsolutePath());
			loopingVideo.prepareAsync();
		} catch (IllegalArgumentException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (SecurityException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (IllegalStateException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

		thumbsDir = getDir("thumbs",Context.MODE_PRIVATE);
		sbThumbs.setOnSeekListener(new OnSeekListener() {

			@Override
			public void onSeeking(long seekingTime) {
				loopingVideo.pause();
				loopingVideo.seekTo((int) seekingTime);
			}

			@Override
			public void onSeeked(long seekedTime) {
				loopingVideo.seekTo((int) seekedTime);
				loopingVideo.start();
			}
		});
		sbThumbs.setBeltListener(new BeltListener() {

			@Override
			public void onBeltRightResizing(int time) {
				// TODO Auto-generated method stub
				Log.d("belt", "resizing right : "+time);
				loopingVideo.pause();
				loopingVideo.seekTo(time);
			}

			@Override
			public void onBeltResized(int leftTime, int rightTime) {
				// TODO Auto-generated method stub
				Log.d("belt", "resized left : "+leftTime+" right : "+rightTime);
				if(loopingVideo.getMode() == LoopingVideoView.PLAY_MODE_REVERSE)
					loopingVideo.seekTo(rightTime);
				else
					loopingVideo.seekTo(leftTime);
				loopingVideo.setLoopingTime(leftTime, rightTime);
				loopingVideo.start();
			}

			@Override
			public void onBeltMoved(int leftTime, int rightTime) {
				// TODO Auto-generated method stub
				loopingVideo.setLoopingTime(leftTime, rightTime);
			}

			@Override
			public void onBeltLeftResizing(int time) {
				// TODO Auto-generated method stub
				Log.d("belt", "resizing left : "+time);
				loopingVideo.pause();
				loopingVideo.seekTo(time);
			}
		});
		sbThumbs.post(new Runnable() {

			@Override
			public void run() {
				// TODO Auto-generated method stub
				sbThumbs.startExtractThumbs(thumbsDir.getAbsolutePath(), videoUrl, duration, 100, duration/10);
				sbThumbs.setBelt(300);
				sbThumbs.setBeltHighlist(true);
				sbThumbs.enableBeltTransform(1000, 10000, 30);
				sbThumbs.setSeekBarImage(BitmapFactory.decodeResource(getResources(), R.drawable.upload_config_belt_thumb),
						getResources().getDimensionPixelSize(R.dimen.upload_video_config_belt_thumb), sbThumbs.getHeight());
				Belt belt = sbThumbs.getBelt();
				belt.setTopImage(BitmapFactory.decodeResource(getResources(), R.drawable.upload_cut_belt_top_pttn),
						getResources().getDimensionPixelSize(R.dimen.upload_video_config_film_belt_bar_width));
				belt.setBottomImage(BitmapFactory.decodeResource(getResources(),R.drawable.upload_cut_belt_bottom),
						getResources().getDimensionPixelSize(R.dimen.upload_video_config_film_belt_bar_width));
				belt.setLeftImage(BitmapFactory.decodeResource(getResources(), R.drawable.upload_cut_tuning_belt_left),
						getResources().getDimensionPixelSize(R.dimen.upload_video_config_film_belt_side_bar_width));
				belt.setRightImage(BitmapFactory.decodeResource(getResources(), R.drawable.upload_cut_tuning_belt_right),
						getResources().getDimensionPixelSize(R.dimen.upload_video_config_film_belt_side_bar_width));
				belt.setShadow(getResources().getDimensionPixelSize(R.dimen.upload_video_cut_belt_shadow));
				sbThumbs.setCurrentTime(0, 1);
//				DisplayMetrics metrics = new DisplayMetrics();
//				getWindowManager().getDefaultDisplay().getMetrics(metrics);
//				float logicalDensity = metrics.density;
//				int w = (int)Math.ceil( logicalDensity * 0.75);
//				sbThumbs.setDividerWidth(w);
			}
		});
	}

	@Override
	public void onDestroy() {
		// TODO Auto-generated method stub
		loopingVideo.recycle();
		super.onDestroy();
	}

	public void restore(){
		if(loopingVideo.getVisibility() != View.VISIBLE)
			loopingVideo.setVisibility(View.VISIBLE);
		loopingVideo.start();//이전 상태로 플레이
	}
	public void recycle(){
		loopingVideo.recycle();
	}

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		// TODO Auto-generated method stub
		menu.clear();
		MenuItem itemComplete = menu.add(Menu.NONE, ITEM_CONFIG_COMPLETE, Menu.NONE, "cut_complete");
		itemComplete.setIcon(R.drawable.upload_video_complete_button);
		itemComplete.setShowAsAction(MenuItem.SHOW_AS_ACTION_ALWAYS);
		
		return true;
	}
	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		// TODO Auto-generated method stub
		switch (item.getItemId()) {
		case ITEM_CONFIG_COMPLETE:
			Toast.makeText(this, "complete", Toast.LENGTH_SHORT).show();
			Intent intent = new Intent(this, UploadPostActivity.class);
			intent.putExtra("url", videoUrl);
			intent.putExtra("jpegsDir", jpegsDir.getAbsolutePath());
			intent.putExtra("isSoundOn", isSoundOn);
			intent.putExtra("playMode", loopingVideo.getMode());
			intent.putExtra("playSpeed", loopingVideo.getPlaySpeed());
			startActivity(intent);
			loopingVideo.pause();
			//			loopingVideo.setVisibility(View.INVISIBLE);
			break;
		}
		return false;
	}
	private void soundOn(){
		isSoundOn = true;
		mp.setVolume(1, 1);
		btSoundConfig.setImageResource(R.drawable.upload_sound_on_button);

	}
	private void soundOff(){
		isSoundOn = false;
		mp.setVolume(0, 0);
		btSoundConfig.setImageResource(R.drawable.upload_sound_off_button);
	}
	private void soundDisable(){
		soundEnable = false;
		mp.setVolume(0, 0);
		btSoundConfig.setImageResource(R.drawable.upload_sound_disable_button);
	}
}
