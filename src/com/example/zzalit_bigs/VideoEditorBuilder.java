package com.example.zzalit_bigs;

import android.content.Context;

public class VideoEditorBuilder {
	
	private VideoEditor encoder = null;
	public VideoEditorBuilder(Context context){
		encoder = new VideoEditor(context);
	}
	
	public VideoEditorBuilder setSize(int width, int height){
		encoder.setSize(width, height);
		return this;
	}
	
	public VideoEditorBuilder onlyKeyFrame(boolean keyFrame){
		encoder.onlyKeyFrame(keyFrame);
		return this;
	}

	public VideoEditorBuilder reverse(boolean reverse){
		encoder.reverse(reverse);
		return this;
	}
	public VideoEditorBuilder setWorkingDirectory(String dir){
		encoder.setWorkingDirectory(dir);
		return this;
	}
	public VideoEditorBuilder silent(boolean silent){
		encoder.silent(silent);
		return this;
	}
	public VideoEditorBuilder setPlaySpeed(float rate){
		encoder.setPlaySpeed(rate);
		return this;
	}
	public VideoEditorBuilder images(boolean isImages){
		encoder.images(isImages);
		return this;
	}
	public VideoEditorBuilder setFps(float fps){
		encoder.setFps(fps);
		return this;
	}
	/**
	 * 일부분만을 추출할 수 있다.
	 * 단위는 초단위이며, 소숫점을 통해 밀리초까지 설정 가능하다.
	 * @param startSec 추출 시작시간
	 * @param durSec 시작시간으로 부터 몇초까지 자를건지
	 */
	public VideoEditorBuilder setExtractTime(float startSec, float durSec){
		encoder.setExtractTime(startSec, durSec);
		return this;
	}
	
	public VideoEditor build(){
		return encoder;
	}
}
