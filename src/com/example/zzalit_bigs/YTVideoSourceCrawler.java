package com.example.zzalit_bigs;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.UnknownHostException;
import java.util.Collections;
import java.util.Comparator;
import java.util.HashMap;
import java.util.Vector;

import org.apache.http.HttpEntity;
import org.apache.http.HttpHost;
import org.apache.http.HttpResponse;
import org.apache.http.client.ClientProtocolException;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.client.params.ClientPNames;
import org.apache.http.client.params.CookiePolicy;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.protocol.HttpContext;

import android.util.Log;



/**
 * http://www.youtube.com/watch?v=Cx6eaVeYXOs					4K (Ultra HD)
 * http://www.youtube.com/watch?v=9QFK1cLhytY					Javatar and .NOT
 * http://www.youtube.com/watch?v=Mt7zsortIXs				 	1080p "Lady Java"
 * http://www.youtube.com/watch?v=WowZLe95WDY					Tom Petty And the Heartbreakers - Learning to Fly (with lyrics)
 * http://www.youtube.com/watch?v=86OfBExGSE0					URZ 720p
 * http://www.youtube.com/watch?v=cNOP2t9FObw 					Blade 360 - 480
 * http://www.youtube.com/watch?v=HvQBrM_i8bU					MZ 1000 Street Fighter
 * http://www.youtube.com/watch?v=yVpbFMhOAwE					How Linx is build
 * http://www.youtube.com/watch?v=4XpnKHJAok8					Tech Talk: Linus Torvalds on git 
 *
 * ODOT http://sourceforge.net/p/ytd2/bugs/7/ http://www.youtube.com/watch?v=fRVVzXnRsUQ   uses RTMPE (http://en.wikipedia.org/wiki/Protected_Streaming), which ytd2 cannot download atm 
 *
 */
public class YTVideoSourceCrawler extends Thread {


	// save diskspace - try to download e.g. 720p before 1080p if HD is set
	public static boolean bSaveDiskSpace = false;

	// ONLY download audio file, the the one with a video stream in it (if available)
	private boolean bAudioOnly = false;

	// append youtube ID (http://www.youtube.com/watch?v=XY) to filename 
	public static boolean bSaveIDinFilename = false;

	public static String sproxy = null;
	public static String[] saargs = null;

	public static String szDLSTATE = "downloading ";

	// something like [http://][www.]youtube.[cc|to|pl|ev|do|ma|in]/watch?v=0123456789A 
	public static final String szYTREGEX = "(?i)^(http(s)?://)?(www\\.)?youtube\\..{2,5}/watch\\?v=[^&]{11}"; // http://de.wikipedia.org/wiki/CcTLD
	// something like [http://][*].youtube.[cc|to|pl|ev|do|ma|in]/   the last / is for marking the end of host, it does not belong to the hostpart
	public static final String szYTHOSTREGEX = "(?i)^(http(s)?://)?(.*)\\.(youtube\\..{2,5}|googlevideo\\..{2,5})/"; 

	// RFC-1123 ? hostname [with protocol]	
	public static final String szPROXYREGEX = "(?i)(^(http(s)?://)?([a-z0-9]+:[a-z0-9]+@)?([a-z0-9\\-]{0,61}[a-z0-9])(\\.([a-z0-9\\-]{0,61}[a-z0-9]))*(:[0-90-90-90-9]{1,4})?$)|()";

	// RFC-1738 URL characters - not a regex for a real URL!!
	public static final String szURLREGEX = "(?i)^(http(s)?://)?[a-z0-9;/\\?@=&\\$\\-_\\.+!*\\'\\(\\),%]+$"; // without ":", plus "%"

	private static final String szPLAYLISTREGEX = "(?i)/view_play_list\\?p=([a-z0-9]*)&playnext=[0-9]{1,2}&v=";

	// all characters that do not belong to an HTTP URL - could be written shorter?? (where did I use this?? dont now anymore)
	final String snotsourcecodeurl = "(?i)[^(a-z)^(0-9)^%^&^=^\\.^:^/^\\?^_^-]";

	static int iThreadcount = 0;

	private final String ssourcecodeurl = "http://";
	private final String ssourcecodeuri = "[a-zA-Z0-9%&=\\.]";

	private String sURL = null;				// main URL (youtube start web page)

	private Vector<YTVideoSource> vNextVideoURL = new Vector<YTVideoSource>();	// list of URLs from webpage source
	private String sFileName = null;		// contains the absolute filename
	//CookieStore bcs = null;			// contains cookies after first HTTP GET

	private String 				sContentType = null;
	private BufferedReader		textreader = null;
	private HttpGet				httpget = null;
	private HttpClient			httpclient = null;
	private HttpHost			target = null;
	private HttpContext			localContext = null;
	private HttpResponse		response = null;

	private boolean isCrawling = false;
	private YTURLCrawlingListener listener = null;

	public YTVideoSourceCrawler(String sUrl) {
		// TODO Auto-generated constructor stub
		super();
		this.sURL = sUrl;
	} // YTDownloadThread()

	boolean downloadone(String sURL) {
		boolean rc = false;
		boolean rc204 = false;
		boolean rc302 = false;

		// stop recursion
		try {
			if (sURL.equals("")) return(false);
		} catch (NullPointerException npe) {
			return(false);
		}
		try {
			this.httpclient = new DefaultHttpClient();
			this.httpclient.getParams().setParameter(ClientPNames.COOKIE_POLICY, CookiePolicy.BEST_MATCH);
			this.httpget = new HttpGet( getURI(sURL) );	
			if (sURL.toLowerCase().startsWith("https"))
				this.target = new HttpHost( getHost(sURL), 443, "https" );
			else
				this.target = new HttpHost( getHost(sURL), 80, "http" );
		} catch (Exception e) {
			Log.d("YTD",e.getMessage());
		}
		Log.d("YTD","executing request: ".concat( this.httpget.getRequestLine().toString()) );
		Log.d("YTD","uri: ".concat( this.httpget.getURI().toString()) );
		Log.d("YTD","host: ".concat( this.target.getHostName() ));

		try {
			this.response = this.httpclient.execute(this.target,this.httpget,this.localContext);
		} catch (ClientProtocolException cpe) {
			Log.d("YTD",cpe.getMessage());
		} catch (UnknownHostException uhe) {
			Log.d("YTD",uhe.getMessage());
		} catch (IOException ioe) {
			Log.d("YTD",ioe.getMessage());
		} catch (IllegalStateException ise) {
			Log.d("YTD",ise.getMessage());
		}

		try {
			Log.d("YTD","HTTP response status line:".concat( this.response.getStatusLine().toString()) );
			if (!(rc = this.response.getStatusLine().toString().toLowerCase().matches("^(http)(.*)200(.*)")) & 
					!(rc204 = this.response.getStatusLine().toString().toLowerCase().matches("^(http)(.*)204(.*)")) &
					!(rc302 = this.response.getStatusLine().toString().toLowerCase().matches("^(http)(.*)302(.*)"))) {
				Log.d("YTD",this.response.getStatusLine().toString().concat(" ").concat(sURL));
				return(rc & rc204 & rc302);
			}
			if (rc204) {
				Log.d("YTD","last response code==204 - download: ".concat(this.vNextVideoURL.get(0).getsYTID()));
				rc = downloadone(this.vNextVideoURL.get(0).getsUrl());
				return(rc);
			}
			if (rc302) 
				Log.d("YTD","location from HTTP Header: ".concat(this.response.getFirstHeader("Location").toString()));

		} catch (NullPointerException npe) {
		}

		HttpEntity entity = null;
		try {
			entity = this.response.getEntity();
		} catch (NullPointerException npe) {
		}

		// try to read HTTP response body
		if (entity != null) {
			try {
				if (this.response.getFirstHeader("Content-Type").getValue().toLowerCase().matches("^text/html(.*)"))
					this.textreader = new BufferedReader(new InputStreamReader(entity.getContent()));
			} catch (IllegalStateException e1) {
				e1.printStackTrace();
			} catch (IOException e1) {
				e1.printStackTrace();
			}
			try {
				this.sContentType = this.response.getFirstHeader("Content-Type").getValue().toLowerCase();
				if (this.sContentType.matches("^text/html(.*)")) {
					rc = savetextdata();
					// test if we got the binary content
				} else { // content-type is not video/
					rc = false;
				}
			} catch (IOException ex) {
				try {
					throw ex;
				} catch (IOException e) {
					e.printStackTrace();
				}
			} catch (RuntimeException ex) {
				try {
					throw ex;
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		} //if (entity != null)

		this.httpclient.getConnectionManager().shutdown();

		Log.d("YTD","done: ".concat(sURL));
		return(rc);
	} // downloadone()

	private int addMPEG_ULTRAHD_Urls(int iindex, HashMap<String, String> ssourcecodevideourls) {
		int inewiindex = iindex;

		this.vNextVideoURL.add(inewiindex++, new YTVideoSource(ssourcecodevideourls.get("138"),this.sURL)); // mpeg ultra HD // new itags > 102

		return inewiindex;
	} // addMPEG_ULTRAHD_Urls

	private int addMPEG_HD_Urls(int iindex, HashMap<String, String> ssourcecodevideourls) {
		int inewiindex = iindex;
		this.vNextVideoURL.add(inewiindex++, new YTVideoSource(ssourcecodevideourls.get("84"),this.sURL,"3D")); // mpeg 3D full HD

		this.vNextVideoURL.add(inewiindex++, new YTVideoSource(ssourcecodevideourls.get("137"),this.sURL)); // mpeg full HD // new itags > 102
		this.vNextVideoURL.add(inewiindex++, new YTVideoSource(ssourcecodevideourls.get("136"),this.sURL)); // mpeg full HD
		this.vNextVideoURL.add(inewiindex++, new YTVideoSource(ssourcecodevideourls.get("37"),this.sURL)); // mpeg full HD
		this.vNextVideoURL.add(inewiindex++, new YTVideoSource(ssourcecodevideourls.get("22"),this.sURL)); // mpeg half HD
		return inewiindex;
	} // addMPEG_HD_Urls

	private int addWBEM_HD_Urls(int iindex, HashMap<String, String> ssourcecodevideourls) {
		int inewiindex = iindex;
		this.vNextVideoURL.add(inewiindex++, new YTVideoSource(ssourcecodevideourls.get("100"),this.sURL,"3D")); // webm 3D HD

		this.vNextVideoURL.add(inewiindex++, new YTVideoSource(ssourcecodevideourls.get("46"),this.sURL)); // webm full HD
		this.vNextVideoURL.add(inewiindex++, new YTVideoSource(ssourcecodevideourls.get("45"),this.sURL)); // webm half HD
		return inewiindex;
	} // addWBEM_SD_Urls

	private int addWBEM_SD_Urls(int iindex, HashMap<String, String> ssourcecodevideourls) {
		int inewiindex = iindex;
		this.vNextVideoURL.add(inewiindex++, new YTVideoSource(ssourcecodevideourls.get("102"),this.sURL,"3D")); // webm 3D SD
		this.vNextVideoURL.add(inewiindex++, new YTVideoSource(ssourcecodevideourls.get("44"),this.sURL)); // webm SD
		this.vNextVideoURL.add(inewiindex++, new YTVideoSource(ssourcecodevideourls.get("43"),this.sURL)); // webm SD
		return inewiindex;
	} // addWBEM_SD_Urls

	private int addFLV_SD_Urls(int iindex, HashMap<String, String> ssourcecodevideourls) {
		int inewiindex = iindex;
		this.vNextVideoURL.add(inewiindex++, new YTVideoSource(ssourcecodevideourls.get("35"),this.sURL)); // flv SD
		this.vNextVideoURL.add(inewiindex++, new YTVideoSource(ssourcecodevideourls.get("34"),this.sURL)); // flv SD
		return inewiindex;
	}

	private int addMPEG_SD_Urls(int iindex, HashMap<String, String> ssourcecodevideourls) {
		int inewiindex = iindex;
		this.vNextVideoURL.add(inewiindex++, new YTVideoSource(ssourcecodevideourls.get("82"),this.sURL,"3D")); // mpeg 3D SD
		this.vNextVideoURL.add(inewiindex++, new YTVideoSource(ssourcecodevideourls.get("135"),this.sURL)); // mpeg SD 
		this.vNextVideoURL.add(inewiindex++, new YTVideoSource(ssourcecodevideourls.get("134"),this.sURL)); // mpeg SD
		this.vNextVideoURL.add(inewiindex++, new YTVideoSource(ssourcecodevideourls.get("18"),this.sURL)); // mpeg SD
		return inewiindex;
	} // addMPEG_SD_Urls

	private int addMPEG_LD_Urls(int iindex, HashMap<String, String> ssourcecodevideourls) {
		int inewiindex = iindex;
		this.vNextVideoURL.add(inewiindex++, new YTVideoSource(ssourcecodevideourls.get("133"),this.sURL)); // mpeg LD // new itags > 102
		this.vNextVideoURL.add(inewiindex++, new YTVideoSource(ssourcecodevideourls.get("160"),this.sURL)); // mpeg LD
		this.vNextVideoURL.add(inewiindex++, new YTVideoSource(ssourcecodevideourls.get("36"),this.sURL)); // mpeg LD
		this.vNextVideoURL.add(inewiindex++, new YTVideoSource(ssourcecodevideourls.get("17"),this.sURL)); // mpeg LD
		return inewiindex;
	} // addMPEG_LD_Urls

	private int addFLV_LD_Urls(int iindex, HashMap<String, String> ssourcecodevideourls) {
		int inewiindex = iindex;

		this.vNextVideoURL.add(inewiindex++, new YTVideoSource(ssourcecodevideourls.get("5"),this.sURL)); // flv LD
		return inewiindex;
	}


	boolean savetextdata() throws IOException {
		boolean rc = false;
		// read html lines one by one and search for java script array of video URLs
		String sline = ""; String sline0=""; String sline1="";
		while (sline != null) {
			sline = this.textreader.readLine();
			// FIXME audio is missing .. something goes wrong extracting URLs of webpage source code - split at , is wrong
			if(sline == null)
				continue;
			if (sline.matches(".*(\"adaptive_fmts\":|\"url_encoded_fmt_stream_map\":).*")) { // behind that to strings are the comma separated video urls we use
				rc = true;
				HashMap<String, String> ssourcecodevideourls = new HashMap<String, String>();

				sline = sline.replaceAll(" ", "");
				sline = sline.replace("%25","%");
				sline = sline.replace("\\u0026", "&");
				if (sline.contains("\"url_encoded_fmt_stream_map\":\"")) {
					sline1 = substringBetween(sline.replaceAll(" ", ""),"\"url_encoded_fmt_stream_map\":\"" , "\"");
				}
				sline0 = "";
				if (sline.contains("\"adaptive_fmts\":\"")) {
					sline0 = substringBetween(sline,"\"adaptive_fmts\":\"" , "\"");
				}

				sline = sline0 + "," + sline1;

				// by anonymous
				String[] ssourcecodeyturls = sline.split(",");
				Log.d("YTD","ssourcecodeuturls.length: ".concat(Integer.toString(ssourcecodeyturls.length)));

				for (String urlString : ssourcecodeyturls) {
					// assuming rtmpe is used for all resolutions, if found once - end download
					if (urlString.matches(".*conn=rtmpe.*")) {
						Log.d("YTD","RTMPE found. cannot download this one!");
						//							output(JFCMainClient.isgerman()?"Herunterladen des Videos wegen nicht unterstütztem Protokoll (RTMPE) leider nicht möglich!":"Unable to download video due to unsupported protocol (RTMPE). sry!");
						break;
					}
					String[] fmtUrlPair = urlString.split("url=http",2);
					if(fmtUrlPair.length != 2)
						continue;
					fmtUrlPair[1] = "url=http"+fmtUrlPair[1]+"&"+fmtUrlPair[0];
					// grep itag=xz out and use xy as hash key
					// 2013-02 itag now has up to 3 digits
					fmtUrlPair[0] = fmtUrlPair[1].substring(fmtUrlPair[1].indexOf("itag=")+5, fmtUrlPair[1].indexOf("itag=")+5+1+(fmtUrlPair[1].matches(".*itag=[0-9]{2}.*")?1:0)+(fmtUrlPair[1].matches(".*itag=[0-9]{3}.*")?1:0));
					fmtUrlPair[1] = fmtUrlPair[1].replaceFirst("url=http%3A%2F%2F", "http://");
					fmtUrlPair[1] = fmtUrlPair[1].replaceAll("%3F","?").replaceAll("%2F", "/").replaceAll("%3B",";").replaceAll("%2C",",").replaceAll("%3D","=").replaceAll("%26", "&").replaceAll("%252C", "%2C").replaceAll("sig=", "signature=").replaceAll("&s=", "&signature=").replaceAll("\\?s=", "?signature=");

					// remove duplicate itag=xyz
					// 2014-01-24 - best method would be to test for parameters behind: sparams=algorithm,burst,clen,dur,factor,gir,id,ip,ipbits,itag,lmt,source,upn,expire
					// and ensure every parameter is present in the video URL only once!
					if (countWords(fmtUrlPair[1], "itag=")==2)
						fmtUrlPair[1] = fmtUrlPair[1].replaceFirst("itag=[0-9]{1,3}", "");

					Log.d("YTD","video url: ".concat(fmtUrlPair[1]));

					try {
						ssourcecodevideourls.put(fmtUrlPair[0], fmtUrlPair[1]); // save that URL
					} catch (java.lang.ArrayIndexOutOfBoundsException aioobe) {
					}
				} // for

				int iindex = 0;
				this.vNextVideoURL.removeAllElements();

				Log.d("YTD","ssourcecodevideourls.length: ".concat(Integer.toString(ssourcecodevideourls.size())));
				// figure out what resolution-button is pressed now (!!) and fill list with possible URLs of video currently being processed video

				this.vNextVideoURL.add(iindex++, new YTVideoSource(ssourcecodevideourls.get("140"),this.sURL,"MP4")); // MP4 AUDIO ONLY - file extension well be .mp4

				iindex = addMPEG_ULTRAHD_Urls(iindex, ssourcecodevideourls);

				// there are no FLV HD URLs for now, so at least try mpg,wbem HD then
				iindex = addMPEG_HD_Urls(iindex, ssourcecodevideourls);
				iindex = addWBEM_HD_Urls(iindex, ssourcecodevideourls);

				//$FALL-THROUGH$
				// try to download desired format first, if it's not available we take the other of same res 
				iindex=addMPEG_SD_Urls(iindex, ssourcecodevideourls);

				iindex=addWBEM_SD_Urls(iindex,ssourcecodevideourls);
				iindex=addFLV_SD_Urls(iindex, ssourcecodevideourls);

				//$FALL-THROUGH$

				// TODO this.sFilenameResPart = "(LD)"; // adding LD to filename because HD-Videos are almost already named HD (?)
				iindex = addMPEG_LD_Urls(iindex, ssourcecodevideourls);

				iindex = addFLV_LD_Urls(iindex, ssourcecodevideourls);

				// remove null entries in list - we later try to download the first (index 0) and if it fails the next one (at index 1) and so on
				Vector<YTVideoSource> removingVideos = new Vector<YTVideoSource>();
				for(YTVideoSource src : vNextVideoURL){
					if (src.getsUrl()==null) {removingVideos.add(src); continue;}
					if (src.getsQuality()==null) removingVideos.add(src);
				}
				vNextVideoURL.removeAll(removingVideos);
				Collections.sort(vNextVideoURL, new Comparator<YTVideoSource>(){
					@Override
					public int compare(YTVideoSource lhs, YTVideoSource rhs) {
						// TODO Auto-generated method stub
						return Integer.parseInt(rhs.getsQuality()) - Integer.parseInt(lhs.getsQuality()); 
					}
				});
				for(YTVideoSource src : vNextVideoURL){
					Log.d("test", src.getsITAG()+" "+src.getsQuality());
				}
			}
		} // while
		return rc;
	} // savetextdata()
	String getURI(String sURL) {
		String suri = "/".concat(sURL.replaceFirst(szYTHOSTREGEX, ""));
		return(suri);
	} // getURI

	String getHost(String sURL) {
		// TODO there must be an easier way to distinguish the host from the rest of an URL - probably something in org.apache.http ??
		String shost = sURL.replaceFirst(szYTHOSTREGEX, "");
		shost = sURL.substring(0, sURL.length()-shost.length());
		shost = shost.toLowerCase().replaceFirst("http[s]?://", "").replaceAll("/", "");
		return(shost);
	} // gethost

	String getFileName() {
		if (this.sFileName != null) return this.sFileName; else return("");
	} // getFileName

	void setFileName(String sFileName) {
		this.sFileName = sFileName;
	} // getFileName

	public void run() {
		downloadone(this.sURL);
		if(listener != null)
			listener.onCompleted(vNextVideoURL);
	} // run()
	/**
	 * crawling 중일경우엔 null을 반환
	 */
	public Vector<YTVideoSource> getYTVideoSources(){
		if(isCrawling)
			return null;
		return vNextVideoURL;
	}

	public void setCrawlingListener(YTURLCrawlingListener listener){
		this.listener = listener;
	}

	public static int countWords(String str, String findStr){
		int lastIndex = 0;
		int count =0;

		while(lastIndex != -1){

			lastIndex = str.indexOf(findStr,lastIndex);

			if( lastIndex != -1){
				count ++;
				lastIndex+=findStr.length();
			}
		}
		return count;
	}
	public static String substringBetween(String str, String open, String close) {
		if (str == null || open == null || close == null) {
			return null;
		}
		int start = str.indexOf(open);
		if (start != -1) {
			int end = str.indexOf(close, start + open.length());
			if (end != -1) {
				return str.substring(start + open.length(), end);
			}
		}
		return null;
	}
	public interface YTURLCrawlingListener{
		public void onCompleted(Vector<YTVideoSource> ytvideos);
	}
}