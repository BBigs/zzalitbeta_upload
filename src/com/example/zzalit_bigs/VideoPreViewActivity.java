package com.example.zzalit_bigs;


import android.content.res.Resources;
import android.graphics.Color;
import android.media.MediaPlayer;
import android.media.MediaPlayer.OnCompletionListener;
import android.media.MediaPlayer.OnPreparedListener;
import android.os.Handler;
import android.os.Looper;
import android.os.Message;
import android.view.MotionEvent;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.View.OnTouchListener;
import android.view.ViewGroup;
import android.view.ViewGroup.LayoutParams;
import android.widget.CompoundButton;
import android.widget.CompoundButton.OnCheckedChangeListener;
import android.widget.SeekBar;
import android.widget.SeekBar.OnSeekBarChangeListener;
import android.widget.TextView;
import android.widget.ToggleButton;
import android.widget.VideoView;

import com.actionbarsherlock.app.SherlockFragmentActivity;

public class VideoPreViewActivity extends SherlockFragmentActivity{
	private VideoView videoView = null;
	private ToggleButton tbPlayPause = null;
	private SeekBar sbTimeBar = null;
	private TextView tvTime = null;
	private Handler timer = null;
	private ViewGroup rootLayout = null;
	private ViewGroup controllerLayout = null;
	private ViewGroup videoViewWrapper = null;
	private MediaPlayer mp = null;
	private ZoopLoadingDialogFragment loadingDialog = null;

	long duration = 0;
	protected void onCreate(android.os.Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_video_preview);
		String url = getIntent().getStringExtra("url");

		Resources res = getResources();
		loadingDialog = ZoopLoadingDialogFragment.newInstance(res.getDimensionPixelSize(R.dimen.upload_loading_bg_width), res.getDimensionPixelSize(R.dimen.upload_loading_bg_height),
				res.getDrawable(R.drawable.upload_loading_bg_2), res.getDrawable(R.drawable.upload_loading_wheel),
				res.getDimensionPixelSize(R.dimen.upload_loading_rotating_image_size), res.getDimensionPixelSize(R.dimen.upload_loading_rotating_image_size));
		loadingDialog.show(getSupportFragmentManager(), "video_loading");

		rootLayout = (ViewGroup)findViewById(R.id.layout_video_preview);
		controllerLayout = (ViewGroup)findViewById(R.id.video_preview_controller);
		videoViewWrapper = (ViewGroup)findViewById(R.id.video_preview_wrapper);
		videoView = (VideoView)findViewById(R.id.video_preview);
		tbPlayPause = (ToggleButton)findViewById(R.id.tb_play_pause);
		sbTimeBar = (SeekBar)findViewById(R.id.sb_preview_time_bar);
		tvTime = (TextView)findViewById(R.id.tv_preview_time);


		controllerLayout.setOnTouchListener(new OnTouchListener() {

			@Override
			public boolean onTouch(View v, MotionEvent event) {
				// TODO Auto-generated method stub
				return true;
			}
		});
		rootLayout.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				finish();
			}
		});

		tbPlayPause.setOnCheckedChangeListener(new OnCheckedChangeListener() {
			@Override
			public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
				// TODO Auto-generated method stub
				if(isChecked)
					videoView.start();
				else
					videoView.pause();
			}
		});
		videoView.setOnPreparedListener(new OnPreparedListener() {

			@Override
			public void onPrepared(MediaPlayer mp) {
				// TODO Auto-generated method stub
				VideoPreViewActivity.this.mp = mp;
				float scale = videoViewWrapper.getWidth()/(float)mp.getVideoWidth();
				int scaledHeight = (int) (mp.getVideoHeight()*scale);
				int controllerHeight = getResources().getDimensionPixelSize(R.dimen.upload_video_preview_mc_height);
				if(scaledHeight <= videoViewWrapper.getHeight()-2*controllerHeight){
					LayoutParams params = videoViewWrapper.getLayoutParams();
					params.height = scaledHeight; //사실 params가 reference라서 set안해도 적용되긴함. for 안전
					videoViewWrapper.setLayoutParams(params);
				}else{
					LayoutParams params = videoViewWrapper.getLayoutParams();
					params.height = videoViewWrapper.getHeight()-2*controllerHeight; //사실 params가 reference라서 set안해도 적용되긴함. for 안전
					videoViewWrapper.setLayoutParams(params);
				}
				controllerLayout.setVisibility(View.VISIBLE);
				videoView.setBackgroundColor(Color.TRANSPARENT);

				duration = mp.getDuration()/1000;
				if(duration > 60*60)
					tvTime.setText(String.format("%d:%02d:%02d/%d:%02d:%02d",
							0,0,0,
							duration/(60*60), (duration%(60*60))/60, (duration%(60*60))%60));
				else
					tvTime.setText(String.format("%02d:%02d/%02d:%02d",
							0,0,
							duration/60, duration%60));
				timer = new Handler(Looper.getMainLooper()){
					@Override
					public void handleMessage(Message msg) {
						// TODO Auto-generated method stub
						super.handleMessage(msg);
						setTimeInfoInUI();
						timer.sendEmptyMessageDelayed(0, 1000);
					}
				};
				timer.sendEmptyMessage(0);
				sbTimeBar.setMax((int) duration);
				sbTimeBar.setOnSeekBarChangeListener(new OnSeekBarChangeListener() {

					@Override
					public void onStopTrackingTouch(SeekBar seekBar) {
						// TODO Auto-generated method stub
					}
					@Override
					public void onStartTrackingTouch(SeekBar seekBar) {
						// TODO Auto-generated method stub
					}
					@Override
					public void onProgressChanged(SeekBar seekBar, int progress,//progress는 초단위
							boolean fromUser) {
						// TODO Auto-generated method stub
						if(fromUser){
							setTimeInfoInUI();
							videoView.seekTo(progress*1000);
						}
					}

				});
				if(loadingDialog.getDialog() != null)
					loadingDialog.dismiss();

			}
		});
		videoView.setOnTouchListener(new OnTouchListener() {

			@Override
			public boolean onTouch(View v, MotionEvent event) {
				// TODO Auto-generated method stub
				if(event.getAction() != MotionEvent.ACTION_DOWN)
					return false;
				if(videoView.isPlaying()){
					videoView.pause();
					tbPlayPause.setChecked(false);
				}else{
					videoView.start();
					tbPlayPause.setChecked(true);
				}
				return true;
			}
		});
		videoView.setOnCompletionListener(new OnCompletionListener() {

			@Override
			public void onCompletion(MediaPlayer mp) {
				// TODO Auto-generated method stub
				finish();
			}
		});
		videoView.setVideoPath(url);
		videoView.start();
	}

	protected void setTimeInfoInUI(){
		int pos = videoView.getCurrentPosition()/1000;
		if(duration > 60*60)
			tvTime.setText(String.format("%d:%02d:%02d/%d:%02d:%02d",
					pos/(60*60), (pos%(60*60))/60, (pos%(60*60))%60,
					duration/(60*60), (duration%(60*60))/60, (duration%(60*60))%60));
		else
			tvTime.setText(String.format("%02d:%02d/%02d:%02d",
					pos/60, pos%60,
					duration/60, duration%60));
		sbTimeBar.setProgress(pos);
	}
	@Override
	protected void onDestroy() {
		// TODO Auto-generated method stubd
		if(mp != null)
			mp.release();
		super.onDestroy();
	}
}
