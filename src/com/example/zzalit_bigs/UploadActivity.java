package com.example.zzalit_bigs;

import android.os.Bundle;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.util.Log;
import android.widget.Toast;

import com.actionbarsherlock.app.SherlockFragmentActivity;
import com.actionbarsherlock.view.MenuItem;


public class UploadActivity extends SherlockFragmentActivity{
	public static final String TAG = "UploadActivity";
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_main);
//		FrameLayout frame = (FrameLayout)findViewById(R.id.upload_fragment_frame);
		getSupportActionBar().setDisplayHomeAsUpEnabled(true);
		getSupportActionBar().setDisplayShowHomeEnabled(false);
		if(savedInstanceState == null){
			FragmentManager fm = getSupportFragmentManager();
			fm.beginTransaction().setTransition(FragmentTransaction.TRANSIT_FRAGMENT_OPEN)
				.add(R.id.upload_fragment_frame, new UploadSearchFragment(), UploadSearchFragment.TAG)//new UploadSearchFragment())
				.addToBackStack(null).commit();
		}
	}	
	@Override
	protected void onDestroy() {
		// TODO Auto-generated method stub
		Log.d("test","a destroy");
		super.onDestroy();
	}
	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		// TODO Auto-generated method stub
		switch(item.getItemId()){
		case com.actionbarsherlock.R.id.abs__action_bar_title:
			finish();
		}
		return super.onOptionsItemSelected(item);
	}
	@Override
	public void onBackPressed() {
		// TODO Auto-generated method stub
		super.onBackPressed();
		FragmentManager fm = getSupportFragmentManager();
		Toast.makeText(this, "onbackPressed", Toast.LENGTH_SHORT).show();
		Log.d("bigs", "onBackPressed fragment cnt:"+ fm.getBackStackEntryCount());
		if(fm.getBackStackEntryCount() == 3)
			((UploadVideoConfigFragment)fm.findFragmentByTag(UploadVideoConfigFragment.TAG)).restore();
		if(fm.getBackStackEntryCount() == 2){
			((UploadVideoConfigFragment)fm.findFragmentByTag(UploadVideoConfigFragment.TAG)).recycle();
			((UploadVideoCutFragment)fm.findFragmentByTag(UploadVideoCutFragment.TAG)).restore();
		}
		if(fm.getBackStackEntryCount() <= 0)
			finish();
	}
}
