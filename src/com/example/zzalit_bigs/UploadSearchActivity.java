package com.example.zzalit_bigs;

import java.util.Vector;

import org.springframework.http.HttpStatus;
import org.springframework.web.client.HttpClientErrorException;

import android.content.Context;
import android.content.Intent;
import android.content.res.Resources;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.util.Log;
import android.view.Gravity;
import android.view.KeyEvent;
import android.view.View;
import android.view.Window;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.view.inputmethod.EditorInfo;
import android.view.inputmethod.InputMethodManager;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.FrameLayout.LayoutParams;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.TextView.OnEditorActionListener;

import com.actionbarsherlock.app.ActionBar;
import com.actionbarsherlock.app.SherlockFragmentActivity;
import com.example.zzalit_bigs.YTVideoSourceCrawler.YTURLCrawlingListener;
import com.haarman.listviewanimations.swinginadapters.prepared.SwingBottomInAnimationAdapter;
import com.handmark.pulltorefresh.library.PullToRefreshBase;
import com.handmark.pulltorefresh.library.PullToRefreshBase.Mode;
import com.handmark.pulltorefresh.library.PullToRefreshBase.OnRefreshListener;
import com.handmark.pulltorefresh.library.PullToRefreshListView;
import com.octo.android.robospice.JacksonSpringAndroidSpiceService;
import com.octo.android.robospice.SpiceManager;
import com.octo.android.robospice.persistence.DurationInMillis;
import com.octo.android.robospice.persistence.exception.SpiceException;
import com.octo.android.robospice.request.listener.RequestListener;

public class UploadSearchActivity extends SherlockFragmentActivity{
	public static final String TAG = "UploadSearchActivity";
	public static final int CNT_RESULTS_PER_REQUEST = 30;//1~50

	private SpiceManager spiceManager = null;

	private PullToRefreshListView lvSearchResults = null;
	private UploadSearchResultAdapter listAdapter = null;
	private ImageView ivResult = null;
	private TextView tvResult = null;
	private ZoopLoadingDialogFragment loadingDialog = null;

	private ViewGroup rootView = null;
	private int lastIndex = 0; //불러온 마지막 인덱스, index는 1부터 시작한다 
	private String query = "";
	public UploadSearchActivity(){
		super();
		spiceManager = new SpiceManager(JacksonSpringAndroidSpiceService.class);
	}

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		super.onCreate(savedInstanceState);
		//root
		setContentView(R.layout.activity_upload_search);
		ActionBar actionBar = getSupportActionBar();
		actionBar.setDisplayHomeAsUpEnabled(true);
		actionBar.setDisplayShowHomeEnabled(false);
		actionBar.setTitle("업로드");
		actionBar.setBackgroundDrawable(new ColorDrawable(getResources().getColor(R.color.theme_blue)));

		Resources res = getResources();
		loadingDialog = ZoopLoadingDialogFragment.newInstance(res.getDimensionPixelSize(R.dimen.upload_loading_bg_width), res.getDimensionPixelSize(R.dimen.upload_loading_bg_height),
				res.getDrawable(R.drawable.upload_loading_bg_2), res.getDrawable(R.drawable.upload_loading_wheel),
				res.getDimensionPixelSize(R.dimen.upload_loading_rotating_image_size), res.getDimensionPixelSize(R.dimen.upload_loading_rotating_image_size));
		rootView = (ViewGroup)findViewById(R.id.layout_upload_search);
		//ready, error imageView
		ivResult = (ImageView) rootView.findViewById(R.id.iv_upload_search_result_icon);
		tvResult = (TextView) rootView.findViewById(R.id.tv_upload_search_result_message);
		//ListView
		lvSearchResults = (PullToRefreshListView)rootView.findViewById(R.id.lv_upload_search_result);
		listAdapter = new UploadSearchResultAdapter(this);
		SwingBottomInAnimationAdapter swingAniAdapter = new SwingBottomInAnimationAdapter(listAdapter);
		lvSearchResults.setAdapter(swingAniAdapter);
		swingAniAdapter.setAbsListView(lvSearchResults.getRefreshableView());
		lvSearchResults.setPullToRefreshOverScrollEnabled(false);
		lvSearchResults.setMode(Mode.PULL_FROM_END);
		//		lvSearchResults.setRefreshing(false)
		lvSearchResults.setOnRefreshListener(new OnRefreshListener<ListView>() {
			@Override
			public void onRefresh(PullToRefreshBase<ListView> refreshView) {
				// TODO Auto-generated method stub
				performRequest(query, lastIndex+1);
			}
		});
		lvSearchResults.getRefreshableView().setOnItemClickListener(new OnItemClickListener() {
			@Override
			public void onItemClick(AdapterView<?> parent, View view, int position,
					long id) {
				position -= 1;//이상하게 1높게 반환되네
				Log.d(TAG,"on preview item click "+position);
				final YTVideo video = (YTVideo) listAdapter.getItem(position);
				if(video.getVideoSources() == null)
					loadingDialog.show(getSupportFragmentManager(), "loading");
				video.crawlVideoResources(false, new YTURLCrawlingListener() {

					@Override
					public void onCompleted(Vector<YTVideoSource> ytvideos) {
						// TODO Auto-generated method stub
						String url = video.getProperSourceUrl(480, true);
						if(loadingDialog.getDialog() != null)
							loadingDialog.dismiss();
						if(url == null){
							//재생할 만한 video format이 없음
							//TODO 경고창 띄워줘야함
							Log.d(TAG, "video dont have proper source");
						}else{
							Intent intent = new Intent(UploadSearchActivity.this, VideoPreViewActivity.class);
							intent.putExtra("url", url);
							startActivity(intent);
						}
					}
				});
			}
		});
		//search
		final Button btSearch = (Button)rootView.findViewById(R.id.bt_upload_search);
		final EditText etSearch = (EditText)rootView.findViewById(R.id.et_upload_search);
		btSearch.setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				InputMethodManager imm = (InputMethodManager) getSystemService(Context.INPUT_METHOD_SERVICE);
				imm.hideSoftInputFromWindow(etSearch.getWindowToken(), 0);
				if(query.equals(etSearch.getText().toString()))
					return;
				query = etSearch.getText().toString();
				lastIndex = 0;
				performRequest(query, 1);
				loadingDialog.show(getSupportFragmentManager(), "loading");
			}
		});
		etSearch.setOnEditorActionListener(new OnEditorActionListener() {

			@Override
			public boolean onEditorAction(TextView v, int actionId, KeyEvent event) {
				// TODO Auto-generated method stub
				if(actionId == EditorInfo.IME_ACTION_SEARCH)
					btSearch.performClick();
				return false;
			}
		});
		lvSearchResults.setVisibility(View.INVISIBLE);//초기에 당겼을때  refresh안뜨게 invisible, xml에서 invisible하면 size가 이상하게 초기화됌

	}
	@Override
	protected void onResume() {
		// TODO Auto-generated method stub
		super.onResume();
	}
	@Override
	public void onStart() {
		// TODO Auto-generated method stub
		Log.d("test","f start");
		spiceManager.start(this);
		super.onStart();
	}
	@Override
	public void onStop() {
		// TODO Auto-generated method stub
		Log.d("test","f stop");
		spiceManager.shouldStop();
		super.onStop();
	}
	@Override
	public void onDestroy() {
		// TODO Auto-generated method stub
		Log.d("test","f destroy");
		super.onDestroy();
	}
	public void performRequest(String query, int startIndex){
		VideoInfoRequest request = new VideoInfoRequest(YTVideoList.class, 
				query, startIndex, CNT_RESULTS_PER_REQUEST);
		spiceManager.execute(request, request.getCacheKey(),
				DurationInMillis.ONE_MINUTE*10, new VideoInfoRequestListener());//TODO 30분이나아니라 업로드 액티비티를 나갈때까지 할수없나
	}

	protected void showResultEmpty(){
		ivResult.setImageResource(R.drawable.upload_video_result_empty);
		tvResult.setText(R.string.upload_search_result_message_result_empty);
		lvSearchResults.setVisibility(View.INVISIBLE);
		ivResult.setVisibility(View.VISIBLE);
		tvResult.setVisibility(View.VISIBLE);
	}
	protected void showSearchReady(){//default
		ivResult.setImageResource(R.drawable.upload_video_before_search);
		tvResult.setText(R.string.upload_search_result_message_before);
		lvSearchResults.setVisibility(View.INVISIBLE);
		ivResult.setVisibility(View.VISIBLE);
		tvResult.setVisibility(View.VISIBLE);
	}
	protected void showResultList(){
		lvSearchResults.setVisibility(View.VISIBLE);
		ivResult.setVisibility(View.INVISIBLE);
		tvResult.setVisibility(View.INVISIBLE);

	}
	protected void showSearchError(){
		ivResult.setImageResource(R.drawable.upload_video_error);
		tvResult.setText(R.string.upload_search_result_message_error);
		lvSearchResults.setVisibility(View.INVISIBLE);
		ivResult.setVisibility(View.VISIBLE);
		tvResult.setVisibility(View.VISIBLE);

	}


	protected class VideoInfoRequestListener implements RequestListener<YTVideoList>{

		@Override
		public void onRequestFailure(SpiceException arg0) {
			// TODO Auto-generated method stub
			Log.w(TAG, arg0.getMessage());//failure인 결과도 캐싱되나??TODO
			//끝까지 탐색한 경우 < request 200 OK json에 items가 안담겨있음, 자동 처리됨
			try{
				HttpClientErrorException exception = (HttpClientErrorException)arg0.getCause();

				if(exception != null && exception.getStatusCode().equals(HttpStatus.BAD_REQUEST)){
					if(lvSearchResults.isRefreshing()){// 제한index를 넘어서 탐색한경우(youtube api가 500까지 제한),  < bad request 400 <--아무짓도 안하면 될듯
						lvSearchResults.onRefreshComplete();
					}
					return;
				}
			}catch(Exception e){
				showSearchError();
			}finally{
				if(loadingDialog.getDialog() != null)
					loadingDialog.dismiss();
			}
		}

		@Override
		public void onRequestSuccess(YTVideoList videos) {
			// TODO Auto-generated method stub
			if(lastIndex == 0 && videos.videos.size() == 0)//total item이 0인경우... <request 200 OK... 이건 처리해줘야겠는데 결과가 없다고.
				showResultEmpty();//위 조건 : 좌 - 초기 검색시 , 우 - 검색시 결과물이 0
			else if(lastIndex == 0){//첫 검색시
				listAdapter.clear();
				showResultList();
			}
			lastIndex += videos.videos.size();
			listAdapter.addResults(videos.videos);
			if(lvSearchResults.isRefreshing()){
				lvSearchResults.onRefreshComplete();
			}
			if(loadingDialog.getDialog() != null)
				loadingDialog.dismiss();
		}
	}
}
