package com.example.zzalit_bigs;

import java.io.StringReader;
import java.util.Iterator;
import java.util.LinkedList;

import org.codehaus.jackson.JsonNode;
import org.codehaus.jackson.map.ObjectMapper;
import org.springframework.http.ResponseEntity;
import org.springframework.http.converter.HttpMessageConverter;
import org.springframework.http.converter.StringHttpMessageConverter;
import org.springframework.web.client.HttpClientErrorException;

import android.util.Log;

import com.octo.android.robospice.request.springandroid.SpringAndroidSpiceRequest;

public class VideoInfoRequest extends SpringAndroidSpiceRequest<YTVideoList>
{
	public static final String TAG = "VideoInfoRequest";
	private String query = null;
	private int startIndex = 0;
	private int result_size = 5;
	public VideoInfoRequest(Class<YTVideoList> clazz, String query, int startIndex, int result_size) {
		super(clazz);
		this.query = query;
		this.startIndex = startIndex;
		this.result_size = result_size;
	}

	@Override
	public YTVideoList loadDataFromNetwork() throws Exception {
		// TODO Auto-generated method stub
		//spring rest
		YTVideoList videos = new YTVideoList();
		LinkedList<HttpMessageConverter<?>> converters = new LinkedList<HttpMessageConverter<?>>();
		converters.add(new StringHttpMessageConverter());
		getRestTemplate().setMessageConverters(converters);
		ResponseEntity<String> res = null;
		try{ 
			res = getRestTemplate().getForEntity(getSearchUrl(), String.class);
		}catch(HttpClientErrorException e){
			Log.w(TAG, e.getMessage());
			return videos;
		}
		//jaskon
		ObjectMapper mapper = new ObjectMapper();
		JsonNode root = mapper.readTree(new StringReader(res.getBody()));
		JsonNode items = root.get("data").get("items");
		if(items != null){
			Iterator<JsonNode> nodes = items.getElements();
			while(nodes.hasNext()){
				JsonNode node = nodes.next();
				YTVideo video = new YTVideo(node.get("id").getTextValue(),
						node.get("title").getTextValue(),
						node.get("duration").getLongValue(),
						node.get("thumbnail").get("sqDefault").getTextValue());
				videos.videos.add(video);
			}
		}
		return videos;
	}

	public String getCacheKey(){
		return TAG+query+startIndex+""+result_size;
	}
	
	private String getSearchUrl(){
		return "http://gdata.youtube.com/feeds/api/videos?q="+query+
			"&start-index="+startIndex+"&alt=jsonc&startindex=2&max-results="+result_size+
			"&v=2";
	}
	
}
